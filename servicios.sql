-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-09-2021 a las 02:16:02
-- Versión del servidor: 8.0.26-0ubuntu0.21.04.3
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `servicios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificar`
--

CREATE TABLE `calificar` (
  `id` int UNSIGNED NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'draft',
  `user_created` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `user_updated` char(36) DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `calificacion` int NOT NULL,
  `user` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `calificar`
--

INSERT INTO `calificar` (`id`, `status`, `user_created`, `date_created`, `user_updated`, `date_updated`, `calificacion`, `user`) VALUES
(1, 'published', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:06:43', NULL, NULL, 3, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3'),
(2, 'published', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:08:33', NULL, NULL, 1, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int UNSIGNED NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'draft',
  `user_created` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `user_updated` char(36) DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `img` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `status`, `user_created`, `date_created`, `user_updated`, `date_updated`, `nombre`, `img`) VALUES
(1, 'published', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:36', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:21:12', 'Reparacion camaras', '07c946b2-cef6-4acc-9346-0fc1cd045f70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_activity`
--

CREATE TABLE `directus_activity` (
  `id` int UNSIGNED NOT NULL,
  `action` varchar(45) NOT NULL,
  `user` char(36) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) NOT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `collection` varchar(64) NOT NULL,
  `item` varchar(255) NOT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_activity`
--

INSERT INTO `directus_activity` (`id`, `action`, `user`, `timestamp`, `ip`, `user_agent`, `collection`, `item`, `comment`) VALUES
(1, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:32:40', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(2, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:33:01', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_settings', '1', NULL),
(3, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:33:23', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(4, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:34:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', NULL),
(5, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:34:36', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_settings', '1', NULL),
(6, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:35:09', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', NULL),
(7, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:35:09', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', NULL),
(8, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:35:56', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_settings', '1', NULL),
(9, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:36:22', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(10, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:36:32', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(11, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:38:01', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(12, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:38:24', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(13, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:38:34', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(14, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'servicios', NULL),
(15, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '1', NULL),
(16, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '2', NULL),
(17, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '3', NULL),
(18, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '4', NULL),
(19, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '5', NULL),
(20, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:55:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '6', NULL),
(21, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:56:51', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '7', NULL),
(22, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:58:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '8', NULL),
(23, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:58:56', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', 'd773f7cf-de5a-48c1-9876-3f2736fcc7a7', NULL),
(24, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(25, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:11', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '778eac17-46b3-48d6-bc0b-8de2daba103b', NULL),
(26, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(27, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:30', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(28, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(29, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'categorias', NULL),
(30, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '9', NULL),
(31, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '10', NULL),
(32, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '11', NULL),
(33, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '12', NULL),
(34, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '13', NULL),
(35, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:09:19', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '14', NULL),
(36, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:12:36', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '15', NULL),
(37, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:13:13', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '16', NULL),
(38, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '17', NULL),
(39, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'servicios_categorias', NULL),
(40, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '18', NULL),
(41, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '19', NULL),
(42, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '20', NULL),
(43, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:14:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '21', NULL),
(44, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '07c946b2-cef6-4acc-9346-0fc1cd045f70', NULL),
(45, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:35', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '1', NULL),
(46, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:35', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'categorias', '1', NULL),
(47, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:47', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(48, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:12', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '3a3139d9-198e-4193-90e8-cfbcd4a495b1', NULL),
(49, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '2', NULL),
(50, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '2', NULL),
(51, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '3', NULL),
(52, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '2', NULL),
(53, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'categorias', '1', NULL),
(54, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:17:03', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '20', NULL),
(55, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:17:55', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '20', NULL),
(56, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:18:40', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '17', NULL),
(57, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:19:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '17', NULL),
(58, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:19:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '4', NULL),
(59, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:19:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '3', NULL),
(60, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:19:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '2', NULL),
(61, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:20:22', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'categorias', '1', NULL),
(62, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:20:44', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '4', NULL),
(63, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:20:44', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '2', NULL),
(64, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:20:53', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '2', NULL),
(65, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:21:12', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '1', NULL),
(66, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:21:12', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'categorias', '1', NULL),
(67, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:11', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '5', NULL),
(68, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:11', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(69, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:38', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '1', NULL),
(70, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:54', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_files', '7eba442d-8249-4b56-a207-c7263fe01c82', NULL),
(71, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios_categorias', '6', NULL),
(72, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'servicios', '3', NULL),
(73, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '22', NULL),
(74, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'servicios_directus_users', NULL),
(75, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '23', NULL),
(76, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '24', NULL),
(77, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '25', NULL),
(78, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:24:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '26', NULL),
(79, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:25:48', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '25', NULL),
(80, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'calificar', NULL),
(81, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '27', NULL),
(82, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '28', NULL),
(83, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '29', NULL),
(84, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '30', NULL),
(85, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '31', NULL),
(86, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:38:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '32', NULL),
(87, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:40:32', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '33', NULL),
(88, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:43:48', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '34', NULL),
(89, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:47:13', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '35', NULL),
(90, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:47:47', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'calificar', '1', NULL),
(91, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:47:52', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'calificar', '2', NULL),
(92, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:50:25', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '36', NULL),
(93, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:02:32', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '37', NULL),
(94, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:02:32', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'junction_directus_users_calificar', NULL),
(95, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:02:32', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '38', NULL),
(96, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:02:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '39', NULL),
(97, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:02:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '40', NULL),
(98, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:05:57', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '41', NULL),
(99, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:05:58', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '42', NULL),
(100, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:06:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '42', NULL),
(101, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:06:42', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'calificar', '1', NULL),
(102, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:08:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'calificar', '2', NULL),
(103, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:08:33', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(104, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:09:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '41', NULL),
(105, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:10:04', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '41', NULL),
(106, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_roles', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL),
(107, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '1', NULL),
(108, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '2', NULL),
(109, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '3', NULL),
(110, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '4', NULL),
(111, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '5', NULL),
(112, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '6', NULL),
(113, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '7', NULL),
(114, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '8', NULL),
(115, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '9', NULL),
(116, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '10', NULL),
(117, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '11', NULL),
(118, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:24', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '12', NULL),
(119, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:28', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '13', NULL),
(120, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:16:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '14', NULL),
(121, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:18:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '15', NULL),
(122, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:18:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '16', NULL),
(123, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:18:34', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '17', NULL),
(124, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:18:38', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '18', NULL),
(125, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_roles', '3c6c3c13-6207-48b3-8d6f-0359b252783e', NULL),
(126, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '19', NULL),
(127, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '20', NULL),
(128, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '21', NULL),
(129, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '22', NULL),
(130, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '23', NULL),
(131, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '24', NULL),
(132, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '25', NULL),
(133, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '26', NULL),
(134, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '27', NULL),
(135, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '28', NULL),
(136, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:00', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '29', NULL),
(137, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:11', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '30', NULL),
(138, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '31', NULL),
(139, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:15', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '32', NULL),
(140, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '33', NULL),
(141, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:20', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '34', NULL),
(142, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:25', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '35', NULL),
(143, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:20:26', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '36', NULL),
(144, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:21:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '2d7e2aee-25f3-46ec-ba96-399561b0f19b', NULL),
(145, 'authenticate', '2d7e2aee-25f3-46ec-ba96-399561b0f19b', '2021-09-18 23:21:54', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '2d7e2aee-25f3-46ec-ba96-399561b0f19b', NULL),
(146, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:23:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(147, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'trabajo', NULL),
(148, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '43', NULL),
(149, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '44', NULL),
(150, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '45', NULL),
(151, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '46', NULL),
(152, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:24:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '47', NULL),
(153, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:33:40', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '48', NULL),
(154, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:35:39', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '49', NULL),
(155, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:35:39', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '50', NULL),
(156, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:36:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '51', NULL),
(157, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:36:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '52', NULL),
(158, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:44:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '53', NULL),
(159, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:46:17', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '54', NULL),
(160, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:46:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_collections', 'trabajo_directus_files', NULL),
(161, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:46:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '55', NULL),
(162, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:46:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '56', NULL),
(163, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:46:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '57', NULL),
(164, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:47:38', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '48', NULL),
(165, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'trabajo_directus_files', '1', NULL),
(166, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'trabajo_directus_files', '2', NULL),
(167, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:18', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'trabajo', '1', NULL),
(168, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:43', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '49', NULL),
(169, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:52', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '51', NULL),
(170, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:00:12', '::1', NULL, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(171, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:03:31', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '37', NULL),
(172, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:03:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '37', NULL),
(173, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:04:41', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', 'e6d6370c-b48f-4216-9460-66a80244b90c', NULL),
(174, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:11:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(175, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:11', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '38', NULL),
(176, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:12', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '39', NULL),
(177, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '38', NULL),
(178, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '37', NULL),
(179, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '39', NULL),
(180, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:13:14', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '40', NULL),
(181, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:32:29', '::1', NULL, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(182, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:34:25', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '541fbb17-880d-40c9-a6fd-81452d092211', NULL),
(183, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 00:34:55', '::1', NULL, 'directus_users', '4201194a-2b93-479b-9c30-a525adf569a7', NULL),
(184, 'create', NULL, '2021-09-19 00:35:07', '::1', NULL, 'directus_users', 'a8041a7c-9381-450e-bdc0-adc1645ff7e3', NULL),
(185, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 03:56:09', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '37', NULL),
(186, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 03:56:10', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '38', NULL),
(187, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 03:56:12', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '39', NULL),
(188, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 03:56:13', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '40', NULL),
(189, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:05:44', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '41', NULL),
(190, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:05:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '42', NULL),
(191, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:05:48', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '43', NULL),
(192, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:05:49', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '44', NULL),
(193, 'create', NULL, '2021-09-19 04:06:01', '::1', NULL, 'directus_users', '2dcd3087-f8dd-49d2-a60b-df83e5ce997b', NULL),
(194, 'create', NULL, '2021-09-19 04:26:10', '::1', NULL, 'directus_users', '71cc5666-8e51-4d55-afd7-4f489884ae1e', NULL),
(195, 'create', NULL, '2021-09-19 04:28:04', '::1', NULL, 'directus_users', '79ea20fd-19f2-46f9-b68b-51d09c4db006', NULL),
(196, 'create', NULL, '2021-09-19 04:30:48', '::1', NULL, 'directus_users', '1aa13590-3a06-4ebe-8c34-2ccab0732f17', NULL),
(197, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:31:41', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '42', NULL),
(198, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:31:44', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '43', NULL),
(199, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:31:45', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '44', NULL),
(200, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:32:45', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '41', NULL),
(201, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:36:59', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '41', NULL),
(202, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '1aa13590-3a06-4ebe-8c34-2ccab0732f17', NULL),
(203, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '2dcd3087-f8dd-49d2-a60b-df83e5ce997b', NULL),
(204, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '4201194a-2b93-479b-9c30-a525adf569a7', NULL),
(205, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '541fbb17-880d-40c9-a6fd-81452d092211', NULL),
(206, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '71cc5666-8e51-4d55-afd7-4f489884ae1e', NULL),
(207, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '79ea20fd-19f2-46f9-b68b-51d09c4db006', NULL),
(208, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:43:27', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', 'a8041a7c-9381-450e-bdc0-adc1645ff7e3', NULL),
(209, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 04:46:05', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '41', NULL),
(210, 'create', NULL, '2021-09-19 04:46:14', '::1', NULL, 'directus_users', 'b74b722f-af27-48be-8ca3-221f852b99e4', NULL),
(211, 'create', NULL, '2021-09-19 04:58:15', '::1', NULL, 'directus_users', '4cd18351-c105-4276-8d78-16e817b30ed3', NULL),
(212, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:00:29', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_fields', '58', NULL),
(213, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:01:21', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '41', NULL),
(214, 'create', NULL, '2021-09-19 05:01:27', '::1', NULL, 'directus_users', 'ca46efe5-65dd-4786-8517-fa796ca42463', NULL),
(215, 'create', NULL, '2021-09-19 05:01:58', '::1', NULL, 'directus_users', '035956df-5b71-4bf0-84b9-eabff5ac63e2', NULL),
(216, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:19:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '035956df-5b71-4bf0-84b9-eabff5ac63e2', NULL),
(217, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:19:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '4cd18351-c105-4276-8d78-16e817b30ed3', NULL),
(218, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:19:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', 'b74b722f-af27-48be-8ca3-221f852b99e4', NULL),
(219, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:19:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', 'ca46efe5-65dd-4786-8517-fa796ca42463', NULL),
(220, 'create', NULL, '2021-09-19 05:19:52', '::1', NULL, 'directus_users', 'b7b0f2b1-a96c-4b24-9371-dadef7f83cad', NULL),
(221, 'create', NULL, '2021-09-19 05:20:16', '::1', NULL, 'directus_users', '691fb13e-063e-45f3-806d-c9e6fe2cb48c', NULL),
(222, 'create', NULL, '2021-09-19 05:23:22', '::1', NULL, 'directus_users', '1bfa55b5-9f49-4490-88ad-8fd036454faa', NULL),
(223, 'create', NULL, '2021-09-19 05:24:37', '::1', NULL, 'directus_users', '2f69a561-77fe-4a29-be6b-c10223ab0fa2', NULL);
INSERT INTO `directus_activity` (`id`, `action`, `user`, `timestamp`, `ip`, `user_agent`, `collection`, `item`, `comment`) VALUES
(224, 'create', NULL, '2021-09-19 05:25:54', '::1', NULL, 'directus_users', 'e3e7b677-5de3-4dc2-aa72-1f3abd084dc8', NULL),
(225, 'create', NULL, '2021-09-19 05:27:12', '::1', NULL, 'directus_users', '3e783461-ac88-44cd-8631-8d36a84c79b5', NULL),
(226, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:33:44', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '45', NULL),
(227, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:33:50', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '45', NULL),
(228, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:33:55', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '46', NULL),
(229, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:34:41', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '46', NULL),
(230, 'create', NULL, '2021-09-19 05:34:50', '::1', NULL, 'directus_users', '150cce88-2db2-473e-9618-d5e746946d7c', NULL),
(231, 'create', NULL, '2021-09-19 05:45:03', '::1', NULL, 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(232, 'authenticate', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 05:45:25', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(233, 'update', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 05:46:35', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(234, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:47:37', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(235, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:48:45', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '47', NULL),
(236, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:48:47', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '48', NULL),
(237, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:48:48', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '49', NULL),
(238, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:48:50', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '50', NULL),
(239, 'delete', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:48:56', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '50', NULL),
(240, 'authenticate', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 05:49:30', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(241, 'authenticate', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:53:22', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL),
(242, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:53:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '47', NULL),
(243, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:53:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '48', NULL),
(244, 'create', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:53:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '51', NULL),
(245, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 05:53:46', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_permissions', '49', NULL),
(246, 'authenticate', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 05:54:30', '::ffff:127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0', 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(247, 'update', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-19 06:02:23', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', 'directus_settings', '1', NULL),
(248, 'authenticate', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 06:03:02', '::ffff:127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0', 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL),
(249, 'authenticate', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-19 06:04:17', '::1', NULL, 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_collections`
--

CREATE TABLE `directus_collections` (
  `collection` varchar(64) NOT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `note` text,
  `display_template` varchar(255) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `singleton` tinyint(1) NOT NULL DEFAULT '0',
  `translations` json DEFAULT NULL,
  `archive_field` varchar(64) DEFAULT NULL,
  `archive_app_filter` tinyint(1) NOT NULL DEFAULT '1',
  `archive_value` varchar(255) DEFAULT NULL,
  `unarchive_value` varchar(255) DEFAULT NULL,
  `sort_field` varchar(64) DEFAULT NULL,
  `accountability` varchar(255) DEFAULT 'all',
  `color` varchar(255) DEFAULT NULL,
  `item_duplication_fields` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_collections`
--

INSERT INTO `directus_collections` (`collection`, `icon`, `note`, `display_template`, `hidden`, `singleton`, `translations`, `archive_field`, `archive_app_filter`, `archive_value`, `unarchive_value`, `sort_field`, `accountability`, `color`, `item_duplication_fields`) VALUES
('calificar', NULL, NULL, NULL, 0, 0, NULL, 'status', 1, 'archived', 'draft', NULL, 'all', NULL, NULL),
('categorias', NULL, NULL, NULL, 0, 0, NULL, 'status', 1, 'archived', 'draft', NULL, 'all', NULL, NULL),
('junction_directus_users_calificar', 'import_export', NULL, NULL, 1, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL),
('servicios', NULL, NULL, NULL, 0, 0, NULL, 'status', 1, 'archived', 'draft', NULL, 'all', NULL, NULL),
('servicios_categorias', 'import_export', NULL, NULL, 1, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL),
('servicios_directus_users', 'import_export', NULL, NULL, 1, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL),
('trabajo', NULL, NULL, NULL, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL),
('trabajo_directus_files', 'import_export', NULL, NULL, 1, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_fields`
--

CREATE TABLE `directus_fields` (
  `id` int UNSIGNED NOT NULL,
  `collection` varchar(64) NOT NULL,
  `field` varchar(64) NOT NULL,
  `special` varchar(64) DEFAULT NULL,
  `interface` varchar(64) DEFAULT NULL,
  `options` json DEFAULT NULL,
  `display` varchar(64) DEFAULT NULL,
  `display_options` json DEFAULT NULL,
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int UNSIGNED DEFAULT NULL,
  `width` varchar(30) DEFAULT 'full',
  `group` int UNSIGNED DEFAULT NULL,
  `translations` json DEFAULT NULL,
  `note` text,
  `conditions` json DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_fields`
--

INSERT INTO `directus_fields` (`id`, `collection`, `field`, `special`, `interface`, `options`, `display`, `display_options`, `readonly`, `hidden`, `sort`, `width`, `group`, `translations`, `note`, `conditions`, `required`) VALUES
(1, 'servicios', 'id', NULL, 'input', NULL, NULL, NULL, 1, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(2, 'servicios', 'status', NULL, 'select-dropdown', '{\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}', 'labels', '{\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(3, 'servicios', 'user_created', 'user-created', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(4, 'servicios', 'date_created', 'date-created', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(5, 'servicios', 'user_updated', 'user-updated', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(6, 'servicios', 'date_updated', 'date-updated', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(7, 'servicios', 'nombre', NULL, 'input', NULL, NULL, NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(8, 'servicios', 'img', NULL, 'file-image', NULL, 'image', '{\"circle\": true}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(9, 'categorias', 'id', NULL, 'input', NULL, NULL, NULL, 1, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(10, 'categorias', 'status', NULL, 'select-dropdown', '{\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}', 'labels', '{\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(11, 'categorias', 'user_created', 'user-created', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(12, 'categorias', 'date_created', 'date-created', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(13, 'categorias', 'user_updated', 'user-updated', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(14, 'categorias', 'date_updated', 'date-updated', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(15, 'categorias', 'nombre', NULL, 'input', NULL, 'raw', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(16, 'categorias', 'img', NULL, 'file-image', NULL, 'image', '{\"circle\": true}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(17, 'servicios', 'categoria', 'm2m', 'list-m2m', NULL, 'related-values', '{\"template\": \"{{categorias_id.nombre}}\"}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(18, 'servicios_categorias', 'id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(19, 'servicios_categorias', 'categorias_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(20, 'categorias', 'servicios', 'm2m', 'list-m2m', NULL, 'related-values', '{\"template\": \"{{servicios_id.nombre}}\"}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(21, 'servicios_categorias', 'servicios_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(22, 'servicios', 'users', 'm2m', 'list-m2m', NULL, 'related-values', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(23, 'servicios_directus_users', 'id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(24, 'servicios_directus_users', 'servicios_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(25, 'directus_users', 'servicios', 'm2m', 'list-m2m', NULL, 'related-values', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(26, 'servicios_directus_users', 'directus_users_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(27, 'calificar', 'id', NULL, 'input', NULL, NULL, NULL, 1, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(28, 'calificar', 'status', NULL, 'select-dropdown', '{\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}', 'labels', '{\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(29, 'calificar', 'user_created', 'user-created', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(30, 'calificar', 'date_created', 'date-created', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(31, 'calificar', 'user_updated', 'user-updated', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(32, 'calificar', 'date_updated', 'date-updated', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(33, 'calificar', 'calificacion', NULL, 'slider', '{\"maxValue\": 5, \"minValue\": 0}', 'raw', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(38, 'junction_directus_users_calificar', 'id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(39, 'junction_directus_users_calificar', 'calificar_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(40, 'junction_directus_users_calificar', 'directus_users_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(41, 'calificar', 'user', NULL, 'select-dropdown-m2o', NULL, 'related-values', '{\"template\": \"{{first_name}}\"}', 0, 0, NULL, 'full', NULL, NULL, NULL, '[{}]', 0),
(42, 'directus_users', 'calificar', 'o2m', 'list-o2m', NULL, 'related-values', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(43, 'trabajo', 'id', NULL, 'input', NULL, NULL, NULL, 1, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(44, 'trabajo', 'user_created', 'user-created', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(45, 'trabajo', 'date_created', 'date-created', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(46, 'trabajo', 'user_updated', 'user-updated', 'select-dropdown-m2o', '{\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}', 'user', NULL, 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(47, 'trabajo', 'date_updated', 'date-updated', 'datetime', NULL, 'datetime', '{\"relative\": true}', 1, 1, NULL, 'half', NULL, NULL, NULL, NULL, 0),
(48, 'trabajo', 'descripcion', NULL, 'input-multiline', NULL, 'raw', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(49, 'trabajo', 'pedidopor', NULL, 'select-dropdown-m2o', NULL, 'related-values', '{\"template\": \"{{first_name}}\"}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(50, 'directus_users', 'trabajopedido', 'o2m', 'list-o2m', NULL, NULL, NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(51, 'trabajo', 'realizadopor', NULL, 'select-dropdown-m2o', NULL, 'related-values', '{\"template\": \"{{first_name}}\"}', 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(52, 'directus_users', 'trabajo', 'o2m', 'list-o2m', NULL, NULL, NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(53, 'trabajo', 'estado', NULL, 'input', '{\"max\": 2, \"min\": 0}', 'raw', '{}', 0, 0, NULL, 'full', NULL, NULL, '0 creado 1 aceptado 2 finalizado', NULL, 0),
(54, 'trabajo', 'imagenes', 'files', 'list-m2m', NULL, 'related-values', NULL, 0, 0, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(55, 'trabajo_directus_files', 'id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(56, 'trabajo_directus_files', 'trabajo_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(57, 'trabajo_directus_files', 'directus_files_id', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0),
(58, 'directus_users', 'type', 'boolean', 'boolean', NULL, 'boolean', NULL, 0, 1, NULL, 'full', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_files`
--

CREATE TABLE `directus_files` (
  `id` char(36) NOT NULL,
  `storage` varchar(255) NOT NULL,
  `filename_disk` varchar(255) DEFAULT NULL,
  `filename_download` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `folder` char(36) DEFAULT NULL,
  `uploaded_by` char(36) DEFAULT NULL,
  `uploaded_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` char(36) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `charset` varchar(50) DEFAULT NULL,
  `filesize` bigint DEFAULT NULL,
  `width` int UNSIGNED DEFAULT NULL,
  `height` int UNSIGNED DEFAULT NULL,
  `duration` int UNSIGNED DEFAULT NULL,
  `embed` varchar(200) DEFAULT NULL,
  `description` text,
  `location` text,
  `tags` text,
  `metadata` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_files`
--

INSERT INTO `directus_files` (`id`, `storage`, `filename_disk`, `filename_download`, `title`, `type`, `folder`, `uploaded_by`, `uploaded_on`, `modified_by`, `modified_on`, `charset`, `filesize`, `width`, `height`, `duration`, `embed`, `description`, `location`, `tags`, `metadata`) VALUES
('053bf5fe-b772-4fd7-abb5-3414bd8f4a25', 'local', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25.png', 'logo-final-con-letras-blancas-218x54.png', 'Logo Final Con Letras Blancas 218x54', 'image/png', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:34:33', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 06:35:10', NULL, 3900, 46, 58, NULL, NULL, NULL, NULL, NULL, '{\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 46, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 58}}'),
('07c946b2-cef6-4acc-9346-0fc1cd045f70', 'local', '07c946b2-cef6-4acc-9346-0fc1cd045f70.png', 'CAMIONERTA 2 PNG.png', 'Camionerta 2 Png', 'image/png', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:15:17', NULL, '2021-09-18 22:15:18', NULL, 1551137, 1500, 1000, NULL, NULL, NULL, NULL, NULL, '{\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 1500, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 1000, \"date:create\": \"2021-09-16T21:08:50+00:00\", \"date:modify\": \"2021-09-16T21:08:50+00:00\"}}'),
('3a3139d9-198e-4193-90e8-cfbcd4a495b1', 'local', '3a3139d9-198e-4193-90e8-cfbcd4a495b1.png', 'CAMIONETA PNG.png', 'Camioneta Png', 'image/png', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:16:12', NULL, '2021-09-18 22:16:13', NULL, 7126273, 3000, 2000, NULL, NULL, NULL, NULL, NULL, '{\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 3000, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 2000, \"date:create\": \"2021-09-16T20:50:42+00:00\", \"date:modify\": \"2021-09-16T20:50:42+00:00\"}}'),
('778eac17-46b3-48d6-bc0b-8de2daba103b', 'local', '778eac17-46b3-48d6-bc0b-8de2daba103b.png', 'APARATO CAPO PNG.png', 'Aparato Capo Png', 'image/png', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:11', NULL, '2021-09-18 21:59:11', NULL, 7640730, 6000, 4000, NULL, NULL, NULL, NULL, NULL, '{\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"Palette\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 6000, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 4000}}'),
('7eba442d-8249-4b56-a207-c7263fe01c82', 'local', '7eba442d-8249-4b56-a207-c7263fe01c82.png', 'CAMIONERTA 2 PNG.png', 'Camionerta 2 Png', 'image/png', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:54', NULL, '2021-09-18 22:22:55', NULL, 1551137, 1500, 1000, NULL, NULL, NULL, NULL, NULL, '{\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 1500, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 1000, \"date:create\": \"2021-09-16T21:08:50+00:00\", \"date:modify\": \"2021-09-16T21:08:50+00:00\"}}'),
('d773f7cf-de5a-48c1-9876-3f2736fcc7a7', 'local', 'd773f7cf-de5a-48c1-9876-3f2736fcc7a7.jpg', 'comprimida1.jpg', 'Comprimida1', 'image/jpeg', NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:58:56', NULL, '2021-09-18 21:58:57', NULL, 2555986, 6360, 2953, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_folders`
--

CREATE TABLE `directus_folders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_migrations`
--

CREATE TABLE `directus_migrations` (
  `version` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_migrations`
--

INSERT INTO `directus_migrations` (`version`, `name`, `timestamp`) VALUES
('20201028A', 'Remove Collection Foreign Keys', '2021-09-18 06:31:10'),
('20201029A', 'Remove System Relations', '2021-09-18 06:31:10'),
('20201029B', 'Remove System Collections', '2021-09-18 06:31:10'),
('20201029C', 'Remove System Fields', '2021-09-18 06:31:10'),
('20201105A', 'Add Cascade System Relations', '2021-09-18 06:31:12'),
('20201105B', 'Change Webhook URL Type', '2021-09-18 06:31:12'),
('20210225A', 'Add Relations Sort Field', '2021-09-18 06:31:12'),
('20210304A', 'Remove Locked Fields', '2021-09-18 06:31:12'),
('20210312A', 'Webhooks Collections Text', '2021-09-18 06:31:12'),
('20210331A', 'Add Refresh Interval', '2021-09-18 06:31:12'),
('20210415A', 'Make Filesize Nullable', '2021-09-18 06:31:12'),
('20210416A', 'Add Collections Accountability', '2021-09-18 06:31:12'),
('20210422A', 'Remove Files Interface', '2021-09-18 06:31:12'),
('20210506A', 'Rename Interfaces', '2021-09-18 06:31:12'),
('20210510A', 'Restructure Relations', '2021-09-18 06:31:13'),
('20210518A', 'Add Foreign Key Constraints', '2021-09-18 06:31:13'),
('20210519A', 'Add System Fk Triggers', '2021-09-18 06:31:14'),
('20210521A', 'Add Collections Icon Color', '2021-09-18 06:31:14'),
('20210608A', 'Add Deep Clone Config', '2021-09-18 06:31:14'),
('20210626A', 'Change Filesize Bigint', '2021-09-18 06:31:14'),
('20210716A', 'Add Conditions to Fields', '2021-09-18 06:31:14'),
('20210721A', 'Add Default Folder', '2021-09-18 06:31:14'),
('20210802A', 'Replace Groups', '2021-09-18 06:31:14'),
('20210803A', 'Add Required to Fields', '2021-09-18 06:31:14'),
('20210805A', 'Update Groups', '2021-09-18 06:31:14'),
('20210805B', 'Change Image Metadata Structure', '2021-09-18 06:31:14'),
('20210811A', 'Add Geometry Config', '2021-09-18 06:31:14'),
('20210831A', 'Remove Limit Column', '2021-09-18 06:31:14'),
('20210907A', 'Webhooks Collections Not Null', '2021-09-18 06:31:14'),
('20210910A', 'Move Module Setup', '2021-09-18 06:31:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_permissions`
--

CREATE TABLE `directus_permissions` (
  `id` int UNSIGNED NOT NULL,
  `role` char(36) DEFAULT NULL,
  `collection` varchar(64) NOT NULL,
  `action` varchar(10) NOT NULL,
  `permissions` json DEFAULT NULL,
  `validation` json DEFAULT NULL,
  `presets` json DEFAULT NULL,
  `fields` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_permissions`
--

INSERT INTO `directus_permissions` (`id`, `role`, `collection`, `action`, `permissions`, `validation`, `presets`, `fields`) VALUES
(1, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_files', 'create', '{}', NULL, NULL, '*'),
(2, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_files', 'read', '{}', NULL, NULL, '*'),
(3, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_files', 'update', '{}', NULL, NULL, '*'),
(4, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_files', 'delete', '{}', NULL, NULL, '*'),
(5, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_folders', 'create', '{}', NULL, NULL, '*'),
(6, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_folders', 'read', '{}', NULL, NULL, '*'),
(7, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_folders', 'update', '{}', NULL, NULL, '*'),
(8, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_folders', 'delete', '{}', NULL, NULL, NULL),
(9, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_users', 'read', '{}', NULL, NULL, NULL),
(10, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_users', 'update', '{\"id\": {\"_eq\": \"$CURRENT_USER\"}}', NULL, NULL, 'first_name,last_name,email,password,location,title,description,avatar,language,theme'),
(11, '5c794a99-9d0b-462a-8b0f-747e53434378', 'directus_roles', 'read', '{}', NULL, NULL, '*'),
(12, '5c794a99-9d0b-462a-8b0f-747e53434378', 'calificar', 'create', '{}', '{}', NULL, '*'),
(13, '5c794a99-9d0b-462a-8b0f-747e53434378', 'calificar', 'read', '{}', '{}', NULL, '*'),
(14, '5c794a99-9d0b-462a-8b0f-747e53434378', 'calificar', 'update', '{}', '{}', NULL, '*'),
(15, '5c794a99-9d0b-462a-8b0f-747e53434378', 'servicios_categorias', 'read', '{}', '{}', NULL, '*'),
(16, '5c794a99-9d0b-462a-8b0f-747e53434378', 'servicios', 'read', '{}', '{}', NULL, '*'),
(17, '5c794a99-9d0b-462a-8b0f-747e53434378', 'categorias', 'read', '{}', '{}', NULL, '*'),
(18, '5c794a99-9d0b-462a-8b0f-747e53434378', 'servicios_directus_users', 'read', '{}', '{}', NULL, '*'),
(19, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_files', 'create', '{}', NULL, NULL, '*'),
(20, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_files', 'read', '{}', NULL, NULL, '*'),
(21, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_files', 'update', '{}', NULL, NULL, '*'),
(22, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_files', 'delete', '{}', NULL, NULL, '*'),
(23, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_folders', 'create', '{}', NULL, NULL, '*'),
(24, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_folders', 'read', '{}', NULL, NULL, '*'),
(25, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_folders', 'update', '{}', NULL, NULL, '*'),
(26, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_folders', 'delete', '{}', NULL, NULL, NULL),
(27, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_users', 'read', '{}', NULL, NULL, NULL),
(28, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_users', 'update', '{\"id\": {\"_eq\": \"$CURRENT_USER\"}}', NULL, NULL, 'first_name,last_name,email,password,location,title,description,avatar,language,theme'),
(29, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'directus_roles', 'read', '{}', NULL, NULL, '*'),
(30, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'calificar', 'read', '{}', '{}', NULL, '*'),
(31, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'categorias', 'read', '{}', '{}', NULL, '*'),
(32, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'servicios', 'read', '{}', '{}', NULL, '*'),
(33, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'servicios_categorias', 'read', '{}', '{}', NULL, '*'),
(34, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'servicios_directus_users', 'read', '{}', '{}', NULL, '*'),
(35, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'calificar', 'create', '{}', '{}', NULL, '*'),
(36, '3c6c3c13-6207-48b3-8d6f-0359b252783e', 'calificar', 'update', '{}', '{}', NULL, '*'),
(41, NULL, 'directus_users', 'create', '{}', '{}', NULL, 'first_name,last_name,email,password,role,type'),
(46, NULL, 'directus_users', 'read', NULL, NULL, NULL, 'first_name,last_name,email,type'),
(47, '5c794a99-9d0b-462a-8b0f-747e53434378', 'trabajo', 'create', '{}', '{}', NULL, '*'),
(48, '5c794a99-9d0b-462a-8b0f-747e53434378', 'trabajo', 'read', '{}', '{}', NULL, '*'),
(49, '5c794a99-9d0b-462a-8b0f-747e53434378', 'trabajo', 'update', '{}', '{}', NULL, '*'),
(51, '5c794a99-9d0b-462a-8b0f-747e53434378', 'trabajo', 'delete', '{}', '{}', NULL, '*');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_presets`
--

CREATE TABLE `directus_presets` (
  `id` int UNSIGNED NOT NULL,
  `bookmark` varchar(255) DEFAULT NULL,
  `user` char(36) DEFAULT NULL,
  `role` char(36) DEFAULT NULL,
  `collection` varchar(64) DEFAULT NULL,
  `search` varchar(100) DEFAULT NULL,
  `filters` json DEFAULT NULL,
  `layout` varchar(100) DEFAULT 'tabular',
  `layout_query` json DEFAULT NULL,
  `layout_options` json DEFAULT NULL,
  `refresh_interval` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_presets`
--

INSERT INTO `directus_presets` (`id`, `bookmark`, `user`, `role`, `collection`, `search`, `filters`, `layout`, `layout_query`, `layout_options`, `refresh_interval`) VALUES
(1, NULL, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', NULL, 'directus_users', NULL, NULL, 'tabular', '{\"cards\": {\"sort\": \"email\"}, \"tabular\": {\"page\": 1, \"fields\": [\"email\", \"first_name\", \"last_name\", \"password\", \"servicios\"]}}', '{\"cards\": {\"icon\": \"account_circle\", \"size\": 4, \"title\": \"{{ first_name }} {{ last_name }}\", \"subtitle\": \"{{ email }}\"}}', NULL),
(2, NULL, '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL, 'directus_users', NULL, NULL, 'cards', '{\"cards\": {\"page\": 1, \"sort\": \"email\"}}', '{\"cards\": {\"icon\": \"account_circle\", \"size\": 4, \"title\": \"{{ first_name }} {{ last_name }}\", \"subtitle\": \"{{ email }}\"}}', NULL),
(3, NULL, '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL, 'servicios', NULL, '[{\"key\": \"hide-archived\", \"field\": \"status\", \"value\": \"archived\", \"locked\": true, \"operator\": \"neq\"}]', 'tabular', '{\"tabular\": {\"page\": 1}}', NULL, NULL),
(4, NULL, '1b9851da-0586-493e-8ed3-311ed3c0185e', NULL, 'trabajo', NULL, NULL, 'tabular', '{\"tabular\": {\"page\": 1}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_relations`
--

CREATE TABLE `directus_relations` (
  `id` int UNSIGNED NOT NULL,
  `many_collection` varchar(64) NOT NULL,
  `many_field` varchar(64) NOT NULL,
  `one_collection` varchar(64) DEFAULT NULL,
  `one_field` varchar(64) DEFAULT NULL,
  `one_collection_field` varchar(64) DEFAULT NULL,
  `one_allowed_collections` text,
  `junction_field` varchar(64) DEFAULT NULL,
  `sort_field` varchar(64) DEFAULT NULL,
  `one_deselect_action` varchar(255) NOT NULL DEFAULT 'nullify'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_relations`
--

INSERT INTO `directus_relations` (`id`, `many_collection`, `many_field`, `one_collection`, `one_field`, `one_collection_field`, `one_allowed_collections`, `junction_field`, `sort_field`, `one_deselect_action`) VALUES
(1, 'servicios', 'user_created', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(2, 'servicios', 'user_updated', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(3, 'servicios', 'img', 'directus_files', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(4, 'categorias', 'user_created', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(5, 'categorias', 'user_updated', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(6, 'categorias', 'img', 'directus_files', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(7, 'servicios_categorias', 'servicios_id', 'servicios', 'categoria', NULL, NULL, 'categorias_id', NULL, 'nullify'),
(8, 'servicios_categorias', 'categorias_id', 'categorias', 'servicios', NULL, NULL, 'servicios_id', NULL, 'nullify'),
(9, 'servicios_directus_users', 'directus_users_id', 'directus_users', 'servicios', NULL, NULL, 'servicios_id', NULL, 'nullify'),
(10, 'servicios_directus_users', 'servicios_id', 'servicios', 'users', NULL, NULL, 'directus_users_id', NULL, 'nullify'),
(11, 'calificar', 'user_created', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(12, 'calificar', 'user_updated', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(15, 'junction_directus_users_calificar', 'directus_users_id', 'directus_users', NULL, NULL, NULL, 'calificar_id', NULL, 'nullify'),
(16, 'junction_directus_users_calificar', 'calificar_id', 'calificar', NULL, NULL, NULL, 'directus_users_id', NULL, 'nullify'),
(17, 'calificar', 'user', 'directus_users', 'calificar', NULL, NULL, NULL, NULL, 'nullify'),
(18, 'trabajo', 'user_created', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(19, 'trabajo', 'user_updated', 'directus_users', NULL, NULL, NULL, NULL, NULL, 'nullify'),
(20, 'trabajo', 'pedidopor', 'directus_users', 'trabajopedido', NULL, NULL, NULL, NULL, 'nullify'),
(21, 'trabajo', 'realizadopor', 'directus_users', 'trabajo', NULL, NULL, NULL, NULL, 'nullify'),
(22, 'trabajo_directus_files', 'directus_files_id', 'directus_files', NULL, NULL, NULL, 'trabajo_id', NULL, 'nullify'),
(23, 'trabajo_directus_files', 'trabajo_id', 'trabajo', 'imagenes', NULL, NULL, 'directus_files_id', NULL, 'nullify');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_revisions`
--

CREATE TABLE `directus_revisions` (
  `id` int UNSIGNED NOT NULL,
  `activity` int UNSIGNED NOT NULL,
  `collection` varchar(64) NOT NULL,
  `item` varchar(255) NOT NULL,
  `data` json DEFAULT NULL,
  `delta` json DEFAULT NULL,
  `parent` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_revisions`
--

INSERT INTO `directus_revisions` (`id`, `activity`, `collection`, `item`, `data`, `delta`, `parent`) VALUES
(1, 2, 'directus_settings', '1', '{\"project_color\": \"#F41F1F\"}', '{\"project_color\": \"#F41F1F\"}', NULL),
(2, 4, 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', '{\"type\": \"image/png\", \"title\": \"Logo Final Con Letras Blancas 218x54\", \"storage\": \"local\", \"filename_download\": \"logo-final-con-letras-blancas-218x54.png\"}', '{\"type\": \"image/png\", \"title\": \"Logo Final Con Letras Blancas 218x54\", \"storage\": \"local\", \"filename_download\": \"logo-final-con-letras-blancas-218x54.png\"}', NULL),
(3, 5, 'directus_settings', '1', '{\"id\": 1, \"basemaps\": null, \"custom_css\": null, \"mapbox_key\": null, \"module_bar\": null, \"project_url\": null, \"public_note\": null, \"project_logo\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\", \"project_name\": \"Directus\", \"project_color\": \"#F41F1F\", \"public_background\": null, \"public_foreground\": null, \"auth_login_attempts\": 25, \"auth_password_policy\": null, \"storage_asset_presets\": null, \"storage_default_folder\": null, \"storage_asset_transform\": \"all\"}', '{\"project_logo\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\"}', NULL),
(4, 6, 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', '{\"id\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\", \"tags\": null, \"type\": \"image/png\", \"embed\": null, \"title\": \"Logo Final Con Letras Blancas 218x54\", \"width\": 218, \"folder\": null, \"height\": 54, \"charset\": null, \"storage\": \"local\", \"duration\": null, \"filesize\": 12889, \"location\": null, \"metadata\": {\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 218, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 54}}, \"description\": null, \"modified_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"modified_on\": \"2021-09-18T06:35:10.000Z\", \"uploaded_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"uploaded_on\": \"2021-09-18T06:34:33.000Z\", \"filename_disk\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25.png\", \"filename_download\": \"logo-final-con-letras-blancas-218x54.png\"}', '{\"type\": \"image/png\", \"title\": \"Logo Final Con Letras Blancas 218x54\", \"storage\": \"local\", \"modified_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"modified_on\": \"2021-09-18T06:35:09.727Z\", \"filename_download\": \"logo-final-con-letras-blancas-218x54.png\"}', NULL),
(5, 7, 'directus_files', '053bf5fe-b772-4fd7-abb5-3414bd8f4a25', '{\"id\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\", \"tags\": null, \"type\": \"image/png\", \"embed\": null, \"title\": \"Logo Final Con Letras Blancas 218x54\", \"width\": 46, \"folder\": null, \"height\": 58, \"charset\": null, \"storage\": \"local\", \"duration\": null, \"filesize\": 3900, \"location\": null, \"metadata\": {\"ihdr\": {\"Filter\": \"Adaptive\", \"BitDepth\": 8, \"ColorType\": \"RGB with Alpha\", \"Interlace\": \"Noninterlaced\", \"ImageWidth\": 46, \"Compression\": \"Deflate/Inflate\", \"ImageHeight\": 58}}, \"description\": null, \"modified_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"modified_on\": \"2021-09-18T06:35:10.000Z\", \"uploaded_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"uploaded_on\": \"2021-09-18T06:34:33.000Z\", \"filename_disk\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25.png\", \"filename_download\": \"logo-final-con-letras-blancas-218x54.png\"}', '{\"modified_by\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"modified_on\": \"2021-09-18T06:35:09.771Z\"}', NULL),
(6, 8, 'directus_settings', '1', '{\"id\": 1, \"basemaps\": null, \"custom_css\": null, \"mapbox_key\": null, \"module_bar\": null, \"project_url\": null, \"public_note\": null, \"project_logo\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\", \"project_name\": \"Servicios\", \"project_color\": \"#F41F1F\", \"public_background\": null, \"public_foreground\": null, \"auth_login_attempts\": 25, \"auth_password_policy\": null, \"storage_asset_presets\": null, \"storage_default_folder\": null, \"storage_asset_transform\": \"all\"}', '{\"project_name\": \"Servicios\"}', NULL),
(7, 9, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '{\"id\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"role\": \"baa19142-4f26-4649-af61-887000bc9b33\", \"tags\": null, \"email\": \"eimepe@yahoo.com\", \"theme\": \"auto\", \"title\": null, \"token\": null, \"avatar\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\", \"status\": \"active\", \"language\": \"en-US\", \"location\": null, \"password\": \"**********\", \"last_name\": \"User\", \"last_page\": \"/users/0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"first_name\": \"Admin\", \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-18T06:35:29.000Z\"}', '{\"avatar\": \"053bf5fe-b772-4fd7-abb5-3414bd8f4a25\"}', NULL),
(8, 10, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '{\"id\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"role\": \"baa19142-4f26-4649-af61-887000bc9b33\", \"tags\": null, \"email\": \"eimepe@yahoo.com\", \"theme\": \"auto\", \"title\": null, \"token\": null, \"avatar\": null, \"status\": \"active\", \"language\": \"en-US\", \"location\": null, \"password\": \"**********\", \"last_name\": \"User\", \"last_page\": \"/users/0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"first_name\": \"Admin\", \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-18T06:35:29.000Z\"}', '{\"avatar\": null}', NULL),
(9, 11, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '{\"id\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"role\": \"baa19142-4f26-4649-af61-887000bc9b33\", \"tags\": null, \"email\": \"eimepe@yahoo.com\", \"theme\": \"light\", \"title\": null, \"token\": null, \"avatar\": null, \"status\": \"active\", \"language\": \"es-419\", \"location\": null, \"password\": \"**********\", \"last_name\": \"User\", \"last_page\": \"/users/0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"first_name\": \"Admin\", \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-18T06:36:56.000Z\"}', '{\"theme\": \"light\", \"language\": \"es-419\"}', NULL),
(10, 13, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '{\"id\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"role\": \"baa19142-4f26-4649-af61-887000bc9b33\", \"tags\": null, \"email\": \"eimepe@yahoo.com\", \"theme\": \"dark\", \"title\": null, \"token\": null, \"avatar\": null, \"status\": \"active\", \"language\": \"es-419\", \"location\": null, \"password\": \"**********\", \"last_name\": \"User\", \"last_page\": \"/users/0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"first_name\": \"Admin\", \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-18T06:38:25.000Z\"}', '{\"theme\": \"dark\"}', NULL),
(11, 14, 'directus_collections', 'servicios', '{\"singleton\": false, \"collection\": \"servicios\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', '{\"singleton\": false, \"collection\": \"servicios\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', NULL),
(12, 15, 'directus_fields', '1', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"servicios\"}', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"servicios\"}', NULL),
(13, 16, 'directus_fields', '2', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"servicios\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"servicios\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', NULL),
(14, 17, 'directus_fields', '3', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"servicios\"}', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"servicios\"}', NULL),
(15, 18, 'directus_fields', '4', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"servicios\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"servicios\", \"display_options\": {\"relative\": true}}', NULL),
(16, 19, 'directus_fields', '5', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"servicios\"}', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"servicios\"}', NULL),
(17, 20, 'directus_fields', '6', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"servicios\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"servicios\", \"display_options\": {\"relative\": true}}', NULL),
(18, 21, 'directus_fields', '7', '{\"field\": \"nombre\", \"hidden\": false, \"readonly\": false, \"interface\": \"input\", \"collection\": \"servicios\"}', '{\"field\": \"nombre\", \"hidden\": false, \"readonly\": false, \"interface\": \"input\", \"collection\": \"servicios\"}', NULL),
(19, 22, 'directus_fields', '8', '{\"field\": \"img\", \"hidden\": false, \"display\": \"image\", \"readonly\": false, \"interface\": \"file-image\", \"collection\": \"servicios\", \"display_options\": {\"circle\": true}}', '{\"field\": \"img\", \"hidden\": false, \"display\": \"image\", \"readonly\": false, \"interface\": \"file-image\", \"collection\": \"servicios\", \"display_options\": {\"circle\": true}}', NULL),
(20, 23, 'directus_files', 'd773f7cf-de5a-48c1-9876-3f2736fcc7a7', '{\"type\": \"image/jpeg\", \"title\": \"Comprimida1\", \"storage\": \"local\", \"filename_download\": \"comprimida1.jpg\"}', '{\"type\": \"image/jpeg\", \"title\": \"Comprimida1\", \"storage\": \"local\", \"filename_download\": \"comprimida1.jpg\"}', NULL),
(21, 24, 'servicios', '1', '{\"img\": \"d773f7cf-de5a-48c1-9876-3f2736fcc7a7\", \"nombre\": \"comida\"}', '{\"img\": \"d773f7cf-de5a-48c1-9876-3f2736fcc7a7\", \"nombre\": \"comida\"}', NULL),
(22, 25, 'directus_files', '778eac17-46b3-48d6-bc0b-8de2daba103b', '{\"type\": \"image/png\", \"title\": \"Aparato Capo Png\", \"storage\": \"local\", \"filename_download\": \"APARATO CAPO PNG.png\"}', '{\"type\": \"image/png\", \"title\": \"Aparato Capo Png\", \"storage\": \"local\", \"filename_download\": \"APARATO CAPO PNG.png\"}', NULL),
(23, 26, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"comida\", \"status\": \"draft\", \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T21:59:15.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"date_updated\": \"2021-09-18T21:59:15.075Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(24, 27, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"comida\", \"status\": \"published\", \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T21:59:31.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"status\": \"published\", \"date_updated\": \"2021-09-18T21:59:30.875Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(25, 28, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"comida\", \"status\": \"draft\", \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T21:59:37.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"status\": \"draft\", \"date_updated\": \"2021-09-18T21:59:37.342Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(26, 29, 'directus_collections', 'categorias', '{\"singleton\": false, \"collection\": \"categorias\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', '{\"singleton\": false, \"collection\": \"categorias\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', NULL),
(27, 30, 'directus_fields', '9', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"categorias\"}', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"categorias\"}', NULL),
(28, 31, 'directus_fields', '10', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"categorias\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"categorias\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', NULL),
(29, 32, 'directus_fields', '11', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"categorias\"}', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"categorias\"}', NULL),
(30, 33, 'directus_fields', '12', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"categorias\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"categorias\", \"display_options\": {\"relative\": true}}', NULL),
(31, 34, 'directus_fields', '13', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"categorias\"}', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"categorias\"}', NULL),
(32, 35, 'directus_fields', '14', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"categorias\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"categorias\", \"display_options\": {\"relative\": true}}', NULL),
(33, 36, 'directus_fields', '15', '{\"field\": \"nombre\", \"hidden\": false, \"display\": \"raw\", \"readonly\": false, \"interface\": \"input\", \"collection\": \"categorias\"}', '{\"field\": \"nombre\", \"hidden\": false, \"display\": \"raw\", \"readonly\": false, \"interface\": \"input\", \"collection\": \"categorias\"}', NULL),
(34, 37, 'directus_fields', '16', '{\"field\": \"img\", \"hidden\": false, \"display\": \"image\", \"readonly\": false, \"interface\": \"file-image\", \"collection\": \"categorias\", \"display_options\": {\"circle\": true}}', '{\"field\": \"img\", \"hidden\": false, \"display\": \"image\", \"readonly\": false, \"interface\": \"file-image\", \"collection\": \"categorias\", \"display_options\": {\"circle\": true}}', NULL),
(35, 38, 'directus_fields', '17', '{\"field\": \"categoria\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\"}', '{\"field\": \"categoria\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\"}', NULL),
(36, 39, 'directus_collections', 'servicios_categorias', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', NULL),
(37, 40, 'directus_fields', '18', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', NULL),
(38, 42, 'directus_fields', '20', '{\"field\": \"servicios\", \"special\": \"m2m\", \"interface\": \"list-m2m\", \"collection\": \"categorias\"}', '{\"field\": \"servicios\", \"special\": \"m2m\", \"interface\": \"list-m2m\", \"collection\": \"categorias\"}', NULL),
(39, 41, 'directus_fields', '19', '{\"field\": \"categorias_id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', '{\"field\": \"categorias_id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', NULL),
(40, 43, 'directus_fields', '21', '{\"field\": \"servicios_id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', '{\"field\": \"servicios_id\", \"hidden\": true, \"collection\": \"servicios_categorias\"}', NULL),
(41, 44, 'directus_files', '07c946b2-cef6-4acc-9346-0fc1cd045f70', '{\"type\": \"image/png\", \"title\": \"Camionerta 2 Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONERTA 2 PNG.png\"}', '{\"type\": \"image/png\", \"title\": \"Camionerta 2 Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONERTA 2 PNG.png\"}', NULL),
(42, 45, 'servicios_categorias', '1', '{\"servicios_id\": 1, \"categorias_id\": 1}', '{\"servicios_id\": 1, \"categorias_id\": 1}', 43),
(43, 46, 'categorias', '1', '{\"img\": \"07c946b2-cef6-4acc-9346-0fc1cd045f70\", \"nombre\": \"Camaras\", \"status\": \"published\", \"servicios\": [{\"servicios_id\": 1}]}', '{\"img\": \"07c946b2-cef6-4acc-9346-0fc1cd045f70\", \"nombre\": \"Camaras\", \"status\": \"published\", \"servicios\": [{\"servicios_id\": 1}]}', NULL),
(44, 47, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"comida\", \"status\": \"published\", \"categoria\": [1], \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T22:15:48.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"status\": \"published\", \"date_updated\": \"2021-09-18T22:15:47.547Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(45, 48, 'directus_files', '3a3139d9-198e-4193-90e8-cfbcd4a495b1', '{\"type\": \"image/png\", \"title\": \"Camioneta Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONETA PNG.png\"}', '{\"type\": \"image/png\", \"title\": \"Camioneta Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONETA PNG.png\"}', NULL),
(46, 49, 'servicios_categorias', '2', '{\"servicios_id\": 2, \"categorias_id\": 1}', '{\"servicios_id\": 2, \"categorias_id\": 1}', 47),
(47, 50, 'servicios', '2', '{\"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}', '{\"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}', 48),
(48, 51, 'servicios_categorias', '3', '{\"servicios_id\": {\"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}, \"categorias_id\": \"1\"}', '{\"servicios_id\": {\"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}, \"categorias_id\": \"1\"}', 50),
(49, 52, 'servicios_categorias', '2', '{\"id\": 2, \"servicios_id\": 2, \"categorias_id\": null}', '{\"categorias_id\": null}', 50),
(50, 53, 'categorias', '1', '{\"id\": 1, \"img\": \"07c946b2-cef6-4acc-9346-0fc1cd045f70\", \"nombre\": \"Camaras\", \"status\": \"published\", \"servicios\": [1, 3], \"date_created\": \"2021-09-18T22:15:36.000Z\", \"date_updated\": \"2021-09-18T22:16:32.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"date_updated\": \"2021-09-18T22:16:31.541Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(51, 54, 'directus_fields', '20', '{\"id\": 20, \"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"categorias\", \"conditions\": null, \"translations\": null, \"display_options\": null}', '{\"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"categorias\", \"conditions\": null, \"translations\": null, \"display_options\": null}', NULL),
(52, 55, 'directus_fields', '20', '{\"id\": 20, \"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"categorias\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{servicios_id.nombre}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"categorias\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{servicios_id.nombre}}\"}}', NULL),
(53, 56, 'directus_fields', '17', '{\"id\": 17, \"note\": null, \"sort\": null, \"field\": \"categoria\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{categorias_id.nombre}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"categoria\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{categorias_id.nombre}}\"}}', NULL),
(54, 57, 'directus_fields', '17', '{\"id\": 17, \"note\": null, \"sort\": null, \"field\": \"categoria\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{categorias_id.nombre}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"categoria\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{categorias_id.nombre}}\"}}', NULL),
(55, 58, 'servicios_categorias', '4', '{\"servicios_id\": \"2\", \"categorias_id\": 1}', '{\"servicios_id\": \"2\", \"categorias_id\": 1}', 57),
(56, 59, 'servicios_categorias', '3', '{\"id\": 3, \"servicios_id\": null, \"categorias_id\": 1}', '{\"servicios_id\": null}', 57),
(57, 60, 'servicios', '2', '{\"id\": 2, \"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [2, 4], \"date_created\": \"2021-09-18T22:16:32.000Z\", \"date_updated\": \"2021-09-18T22:20:00.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"date_updated\": \"2021-09-18T22:19:59.553Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(58, 61, 'categorias', '1', '{\"id\": 1, \"img\": \"07c946b2-cef6-4acc-9346-0fc1cd045f70\", \"nombre\": \"Reparacion camaras\", \"status\": \"published\", \"servicios\": [1, 3, 4], \"date_created\": \"2021-09-18T22:15:36.000Z\", \"date_updated\": \"2021-09-18T22:20:23.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"nombre\": \"Reparacion camaras\", \"date_updated\": \"2021-09-18T22:20:22.715Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(59, 62, 'servicios_categorias', '4', '{\"id\": 4, \"servicios_id\": null, \"categorias_id\": 1}', '{\"servicios_id\": null}', 60),
(60, 63, 'servicios', '2', '{\"id\": 2, \"img\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\", \"nombre\": \"cableado camaras\", \"status\": \"published\", \"categoria\": [2], \"date_created\": \"2021-09-18T22:16:32.000Z\", \"date_updated\": \"2021-09-18T22:20:45.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"date_updated\": \"2021-09-18T22:20:44.704Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(61, 65, 'servicios_categorias', '1', '{\"id\": 1, \"servicios_id\": 1, \"categorias_id\": null}', '{\"categorias_id\": null}', 62),
(62, 66, 'categorias', '1', '{\"id\": 1, \"img\": \"07c946b2-cef6-4acc-9346-0fc1cd045f70\", \"nombre\": \"Reparacion camaras\", \"status\": \"published\", \"servicios\": [3, 4], \"date_created\": \"2021-09-18T22:15:36.000Z\", \"date_updated\": \"2021-09-18T22:21:12.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"date_updated\": \"2021-09-18T22:21:12.407Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(63, 67, 'servicios_categorias', '5', '{\"servicios_id\": \"1\", \"categorias_id\": 1}', '{\"servicios_id\": \"1\", \"categorias_id\": 1}', 64),
(64, 68, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"comida\", \"status\": \"published\", \"categoria\": [5], \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T22:22:11.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"date_updated\": \"2021-09-18T22:22:11.444Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(65, 69, 'servicios', '1', '{\"id\": 1, \"img\": \"778eac17-46b3-48d6-bc0b-8de2daba103b\", \"nombre\": \"mantenimiento de camaras\", \"status\": \"published\", \"categoria\": [5], \"date_created\": \"2021-09-18T21:59:01.000Z\", \"date_updated\": \"2021-09-18T22:22:39.000Z\", \"user_created\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', '{\"nombre\": \"mantenimiento de camaras\", \"date_updated\": \"2021-09-18T22:22:38.850Z\", \"user_updated\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\"}', NULL),
(66, 70, 'directus_files', '7eba442d-8249-4b56-a207-c7263fe01c82', '{\"type\": \"image/png\", \"title\": \"Camionerta 2 Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONERTA 2 PNG.png\"}', '{\"type\": \"image/png\", \"title\": \"Camionerta 2 Png\", \"storage\": \"local\", \"filename_download\": \"CAMIONERTA 2 PNG.png\"}', NULL),
(67, 71, 'servicios_categorias', '6', '{\"servicios_id\": 3, \"categorias_id\": 1}', '{\"servicios_id\": 3, \"categorias_id\": 1}', 68),
(68, 72, 'servicios', '3', '{\"img\": \"7eba442d-8249-4b56-a207-c7263fe01c82\", \"nombre\": \"instalacion camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}', '{\"img\": \"7eba442d-8249-4b56-a207-c7263fe01c82\", \"nombre\": \"instalacion camaras\", \"status\": \"published\", \"categoria\": [{\"categorias_id\": 1}]}', NULL),
(69, 73, 'directus_fields', '22', '{\"field\": \"users\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\"}', '{\"field\": \"users\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"servicios\"}', NULL),
(70, 74, 'directus_collections', 'servicios_directus_users', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', NULL),
(71, 75, 'directus_fields', '23', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', NULL),
(72, 77, 'directus_fields', '25', '{\"field\": \"servicios\", \"special\": \"m2m\", \"interface\": \"list-m2m\", \"collection\": \"directus_users\"}', '{\"field\": \"servicios\", \"special\": \"m2m\", \"interface\": \"list-m2m\", \"collection\": \"directus_users\"}', NULL),
(73, 76, 'directus_fields', '24', '{\"field\": \"servicios_id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', '{\"field\": \"servicios_id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', NULL),
(74, 78, 'directus_fields', '26', '{\"field\": \"directus_users_id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', '{\"field\": \"directus_users_id\", \"hidden\": true, \"collection\": \"servicios_directus_users\"}', NULL),
(75, 79, 'directus_fields', '25', '{\"id\": 25, \"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"directus_users\", \"conditions\": null, \"translations\": null, \"display_options\": null}', '{\"note\": null, \"sort\": null, \"field\": \"servicios\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"m2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-m2m\", \"collection\": \"directus_users\", \"conditions\": null, \"translations\": null, \"display_options\": null}', NULL),
(76, 80, 'directus_collections', 'calificar', '{\"singleton\": false, \"collection\": \"calificar\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', '{\"singleton\": false, \"collection\": \"calificar\", \"archive_field\": \"status\", \"archive_value\": \"archived\", \"unarchive_value\": \"draft\"}', NULL),
(77, 81, 'directus_fields', '27', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"calificar\"}', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"calificar\"}', NULL),
(78, 82, 'directus_fields', '28', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"calificar\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', '{\"field\": \"status\", \"width\": \"full\", \"display\": \"labels\", \"options\": {\"choices\": [{\"text\": \"$t:published\", \"value\": \"published\"}, {\"text\": \"$t:draft\", \"value\": \"draft\"}, {\"text\": \"$t:archived\", \"value\": \"archived\"}]}, \"interface\": \"select-dropdown\", \"collection\": \"calificar\", \"display_options\": {\"choices\": [{\"value\": \"published\", \"background\": \"#00C897\"}, {\"value\": \"draft\", \"background\": \"#D3DAE4\"}, {\"value\": \"archived\", \"background\": \"#F7971C\"}], \"showAsDot\": true}}', NULL),
(79, 83, 'directus_fields', '29', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', NULL),
(80, 84, 'directus_fields', '30', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"calificar\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"calificar\", \"display_options\": {\"relative\": true}}', NULL),
(81, 85, 'directus_fields', '31', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', NULL),
(82, 86, 'directus_fields', '32', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"calificar\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"calificar\", \"display_options\": {\"relative\": true}}', NULL),
(83, 87, 'directus_fields', '33', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"raw\", \"options\": {\"maxValue\": 5, \"minValue\": 0}, \"special\": null, \"readonly\": false, \"interface\": \"slider\", \"collection\": \"calificar\", \"display_options\": null}', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"raw\", \"options\": {\"maxValue\": 5, \"minValue\": 0}, \"special\": null, \"readonly\": false, \"interface\": \"slider\", \"collection\": \"calificar\", \"display_options\": null}', NULL),
(84, 88, 'directus_fields', '34', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"directus_users\"}', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"directus_users\"}', NULL),
(85, 89, 'directus_fields', '35', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"directus_users\"}', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"directus_users\"}', NULL),
(86, 90, 'calificar', '1', '{\"status\": \"published\", \"calificacion\": 1}', '{\"status\": \"published\", \"calificacion\": 1}', NULL),
(87, 91, 'calificar', '2', '{\"calificacion\": 2}', '{\"calificacion\": 2}', NULL),
(88, 92, 'directus_fields', '36', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"o2m\", \"readonly\": false, \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"o2m\", \"readonly\": false, \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', NULL),
(89, 93, 'directus_fields', '37', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"directus_users\"}', '{\"field\": \"calificacion\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"m2m\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"directus_users\"}', NULL),
(90, 94, 'directus_collections', 'junction_directus_users_calificar', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', NULL),
(91, 95, 'directus_fields', '38', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', NULL),
(92, 96, 'directus_fields', '39', '{\"field\": \"calificar_id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', '{\"field\": \"calificar_id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', NULL),
(93, 97, 'directus_fields', '40', '{\"field\": \"directus_users_id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', '{\"field\": \"directus_users_id\", \"hidden\": true, \"collection\": \"junction_directus_users_calificar\"}', NULL),
(94, 98, 'directus_fields', '41', '{\"field\": \"user\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', '{\"field\": \"user\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\"}', NULL),
(95, 99, 'directus_fields', '42', '{\"field\": \"calificar\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', '{\"field\": \"calificar\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', NULL),
(96, 100, 'directus_fields', '42', '{\"id\": 42, \"note\": null, \"sort\": null, \"field\": \"calificar\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"o2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-o2m\", \"collection\": \"directus_users\", \"conditions\": null, \"translations\": null, \"display_options\": null}', '{\"note\": null, \"sort\": null, \"field\": \"calificar\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": [\"o2m\"], \"readonly\": false, \"required\": false, \"interface\": \"list-o2m\", \"collection\": \"directus_users\", \"conditions\": null, \"translations\": null, \"display_options\": null}', NULL),
(97, 101, 'calificar', '1', '{\"user\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"status\": \"published\", \"calificacion\": 3}', '{\"user\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"status\": \"published\", \"calificacion\": 3}', NULL),
(98, 102, 'calificar', '2', '{\"user\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"status\": \"published\", \"calificacion\": 1}', '{\"user\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"status\": \"published\", \"calificacion\": 1}', 99),
(99, 103, 'directus_users', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '{\"id\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"role\": \"baa19142-4f26-4649-af61-887000bc9b33\", \"tags\": null, \"email\": \"eimepe@yahoo.com\", \"theme\": \"dark\", \"title\": null, \"token\": null, \"avatar\": null, \"status\": \"active\", \"language\": \"es-419\", \"location\": null, \"password\": \"**********\", \"calificar\": [1, 2], \"last_name\": \"User\", \"last_page\": \"/users/0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"servicios\": [], \"first_name\": \"Admin\", \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-18T23:08:16.000Z\"}', '{}', NULL),
(100, 104, 'directus_fields', '41', '{\"id\": 41, \"note\": null, \"sort\": null, \"field\": \"user\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\", \"conditions\": null, \"translations\": null, \"display_options\": null}', '{\"note\": null, \"sort\": null, \"field\": \"user\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\", \"conditions\": null, \"translations\": null, \"display_options\": null}', NULL),
(101, 105, 'directus_fields', '41', '{\"id\": 41, \"note\": null, \"sort\": null, \"field\": \"user\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\", \"conditions\": [{}], \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"user\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"calificar\", \"conditions\": [{}], \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', NULL),
(102, 106, 'directus_roles', '5c794a99-9d0b-462a-8b0f-747e53434378', '{\"name\": \"cliente\", \"app_access\": true, \"admin_access\": false}', '{\"name\": \"cliente\", \"app_access\": true, \"admin_access\": false}', NULL),
(103, 107, 'directus_permissions', '1', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(104, 108, 'directus_permissions', '2', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(105, 109, 'directus_permissions', '3', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(106, 110, 'directus_permissions', '4', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(107, 111, 'directus_permissions', '5', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(108, 112, 'directus_permissions', '6', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(109, 113, 'directus_permissions', '7', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(110, 114, 'directus_permissions', '8', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(111, 115, 'directus_permissions', '9', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"collection\": \"directus_users\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"collection\": \"directus_users\", \"permissions\": {}}', NULL),
(112, 116, 'directus_permissions', '10', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"first_name,last_name,email,password,location,title,description,avatar,language,theme\", \"collection\": \"directus_users\", \"permissions\": {\"id\": {\"_eq\": \"$CURRENT_USER\"}}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": \"first_name,last_name,email,password,location,title,description,avatar,language,theme\", \"collection\": \"directus_users\", \"permissions\": {\"id\": {\"_eq\": \"$CURRENT_USER\"}}}', NULL);
INSERT INTO `directus_revisions` (`id`, `activity`, `collection`, `item`, `data`, `delta`, `parent`) VALUES
(113, 117, 'directus_permissions', '11', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_roles\", \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_roles\", \"permissions\": {}}', NULL),
(114, 118, 'directus_permissions', '12', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(115, 119, 'directus_permissions', '13', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(116, 120, 'directus_permissions', '14', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(117, 121, 'directus_permissions', '15', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_categorias\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_categorias\", \"validation\": {}, \"permissions\": {}}', NULL),
(118, 122, 'directus_permissions', '16', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios\", \"validation\": {}, \"permissions\": {}}', NULL),
(119, 123, 'directus_permissions', '17', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"categorias\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"categorias\", \"validation\": {}, \"permissions\": {}}', NULL),
(120, 124, 'directus_permissions', '18', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(121, 125, 'directus_roles', '3c6c3c13-6207-48b3-8d6f-0359b252783e', '{\"name\": \"tecnico\", \"app_access\": true, \"admin_access\": false}', '{\"name\": \"tecnico\", \"app_access\": true, \"admin_access\": false}', NULL),
(122, 126, 'directus_permissions', '19', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(123, 127, 'directus_permissions', '20', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(124, 128, 'directus_permissions', '21', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(125, 129, 'directus_permissions', '22', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"delete\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"delete\", \"fields\": \"*\", \"collection\": \"directus_files\", \"permissions\": {}}', NULL),
(126, 130, 'directus_permissions', '23', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(127, 131, 'directus_permissions', '24', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(128, 132, 'directus_permissions', '25', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"*\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(129, 133, 'directus_permissions', '26', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"delete\", \"collection\": \"directus_folders\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"delete\", \"collection\": \"directus_folders\", \"permissions\": {}}', NULL),
(130, 134, 'directus_permissions', '27', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"collection\": \"directus_users\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"collection\": \"directus_users\", \"permissions\": {}}', NULL),
(131, 135, 'directus_permissions', '28', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"first_name,last_name,email,password,location,title,description,avatar,language,theme\", \"collection\": \"directus_users\", \"permissions\": {\"id\": {\"_eq\": \"$CURRENT_USER\"}}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": \"first_name,last_name,email,password,location,title,description,avatar,language,theme\", \"collection\": \"directus_users\", \"permissions\": {\"id\": {\"_eq\": \"$CURRENT_USER\"}}}', NULL),
(132, 136, 'directus_permissions', '29', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_roles\", \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": \"*\", \"collection\": \"directus_roles\", \"permissions\": {}}', NULL),
(133, 137, 'directus_permissions', '30', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(134, 138, 'directus_permissions', '31', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"categorias\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"categorias\", \"validation\": {}, \"permissions\": {}}', NULL),
(135, 139, 'directus_permissions', '32', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios\", \"validation\": {}, \"permissions\": {}}', NULL),
(136, 140, 'directus_permissions', '33', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_categorias\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_categorias\", \"validation\": {}, \"permissions\": {}}', NULL),
(137, 141, 'directus_permissions', '34', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"servicios_directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(138, 142, 'directus_permissions', '35', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(139, 143, 'directus_permissions', '36', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"calificar\", \"validation\": {}, \"permissions\": {}}', NULL),
(140, 144, 'directus_users', '2d7e2aee-25f3-46ec-ba96-399561b0f19b', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"eimepe2012@gmail.com\", \"language\": \"es-419\", \"password\": \"**********\", \"last_name\": \"mejia\", \"first_name\": \"eider\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"eimepe2012@gmail.com\", \"language\": \"es-419\", \"password\": \"**********\", \"last_name\": \"mejia\", \"first_name\": \"eider\"}', NULL),
(141, 147, 'directus_collections', 'trabajo', '{\"singleton\": false, \"collection\": \"trabajo\"}', '{\"singleton\": false, \"collection\": \"trabajo\"}', NULL),
(142, 148, 'directus_fields', '43', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"trabajo\"}', '{\"field\": \"id\", \"hidden\": true, \"readonly\": true, \"interface\": \"input\", \"collection\": \"trabajo\"}', NULL),
(143, 149, 'directus_fields', '44', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', '{\"field\": \"user_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-created\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', NULL),
(144, 150, 'directus_fields', '45', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"trabajo\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_created\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-created\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"trabajo\", \"display_options\": {\"relative\": true}}', NULL),
(145, 151, 'directus_fields', '46', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', '{\"field\": \"user_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"user\", \"options\": {\"template\": \"{{avatar.$thumbnail}} {{first_name}} {{last_name}}\"}, \"special\": \"user-updated\", \"readonly\": true, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', NULL),
(146, 152, 'directus_fields', '47', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"trabajo\", \"display_options\": {\"relative\": true}}', '{\"field\": \"date_updated\", \"width\": \"half\", \"hidden\": true, \"display\": \"datetime\", \"special\": \"date-updated\", \"readonly\": true, \"interface\": \"datetime\", \"collection\": \"trabajo\", \"display_options\": {\"relative\": true}}', NULL),
(147, 153, 'directus_fields', '48', '{\"field\": \"descripcion\", \"hidden\": false, \"display\": \"raw\", \"options\": null, \"special\": null, \"readonly\": false, \"interface\": \"input-rich-text-html\", \"collection\": \"trabajo\", \"display_options\": null}', '{\"field\": \"descripcion\", \"hidden\": false, \"display\": \"raw\", \"options\": null, \"special\": null, \"readonly\": false, \"interface\": \"input-rich-text-html\", \"collection\": \"trabajo\", \"display_options\": null}', NULL),
(148, 154, 'directus_fields', '49', '{\"field\": \"pedidopor\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', '{\"field\": \"pedidopor\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', NULL),
(149, 155, 'directus_fields', '50', '{\"field\": \"trabajopedido\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', '{\"field\": \"trabajopedido\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', NULL),
(150, 156, 'directus_fields', '51', '{\"field\": \"realizadopor\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', '{\"field\": \"realizadopor\", \"hidden\": false, \"display\": \"related-values\", \"readonly\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\"}', NULL),
(151, 157, 'directus_fields', '52', '{\"field\": \"trabajo\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', '{\"field\": \"trabajo\", \"special\": \"o2m\", \"interface\": \"list-o2m\", \"collection\": \"directus_users\"}', NULL),
(152, 158, 'directus_fields', '53', '{\"note\": \"0 creado 1 aceptado 2 finalizado\", \"field\": \"estado\", \"hidden\": false, \"display\": \"raw\", \"options\": {\"max\": 2, \"min\": 0}, \"special\": null, \"readonly\": false, \"interface\": \"input\", \"collection\": \"trabajo\", \"conditions\": null, \"display_options\": {}}', '{\"note\": \"0 creado 1 aceptado 2 finalizado\", \"field\": \"estado\", \"hidden\": false, \"display\": \"raw\", \"options\": {\"max\": 2, \"min\": 0}, \"special\": null, \"readonly\": false, \"interface\": \"input\", \"collection\": \"trabajo\", \"conditions\": null, \"display_options\": {}}', NULL),
(153, 159, 'directus_fields', '54', '{\"field\": \"imagenes\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"files\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"trabajo\"}', '{\"field\": \"imagenes\", \"hidden\": false, \"display\": \"related-values\", \"special\": \"files\", \"readonly\": false, \"interface\": \"list-m2m\", \"collection\": \"trabajo\"}', NULL),
(154, 160, 'directus_collections', 'trabajo_directus_files', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', '{\"icon\": \"import_export\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', NULL),
(155, 161, 'directus_fields', '55', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', '{\"field\": \"id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', NULL),
(156, 162, 'directus_fields', '56', '{\"field\": \"trabajo_id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', '{\"field\": \"trabajo_id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', NULL),
(157, 163, 'directus_fields', '57', '{\"field\": \"directus_files_id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', '{\"field\": \"directus_files_id\", \"hidden\": true, \"collection\": \"trabajo_directus_files\"}', NULL),
(158, 164, 'directus_fields', '48', '{\"id\": 48, \"note\": null, \"sort\": null, \"field\": \"descripcion\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"raw\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"input-multiline\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": null}', '{\"note\": null, \"sort\": null, \"field\": \"descripcion\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"raw\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"input-multiline\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": null}', NULL),
(159, 165, 'trabajo_directus_files', '1', '{\"trabajo_id\": 1, \"directus_files_id\": \"7eba442d-8249-4b56-a207-c7263fe01c82\"}', '{\"trabajo_id\": 1, \"directus_files_id\": \"7eba442d-8249-4b56-a207-c7263fe01c82\"}', 161),
(160, 166, 'trabajo_directus_files', '2', '{\"trabajo_id\": 1, \"directus_files_id\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\"}', '{\"trabajo_id\": 1, \"directus_files_id\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\"}', 161),
(161, 167, 'trabajo', '1', '{\"imagenes\": [{\"directus_files_id\": \"7eba442d-8249-4b56-a207-c7263fe01c82\"}, {\"directus_files_id\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\"}], \"pedidopor\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"descripcion\": \"esto es un trabajo de prueba\", \"realizadopor\": \"2d7e2aee-25f3-46ec-ba96-399561b0f19b\"}', '{\"imagenes\": [{\"directus_files_id\": \"7eba442d-8249-4b56-a207-c7263fe01c82\"}, {\"directus_files_id\": \"3a3139d9-198e-4193-90e8-cfbcd4a495b1\"}], \"pedidopor\": \"0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3\", \"descripcion\": \"esto es un trabajo de prueba\", \"realizadopor\": \"2d7e2aee-25f3-46ec-ba96-399561b0f19b\"}', NULL),
(162, 168, 'directus_fields', '49', '{\"id\": 49, \"note\": null, \"sort\": null, \"field\": \"pedidopor\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"pedidopor\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', NULL),
(163, 169, 'directus_fields', '51', '{\"id\": 51, \"note\": null, \"sort\": null, \"field\": \"realizadopor\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', '{\"note\": null, \"sort\": null, \"field\": \"realizadopor\", \"group\": null, \"width\": \"full\", \"hidden\": false, \"display\": \"related-values\", \"options\": null, \"special\": null, \"readonly\": false, \"required\": false, \"interface\": \"select-dropdown-m2o\", \"collection\": \"trabajo\", \"conditions\": null, \"translations\": null, \"display_options\": {\"template\": \"{{first_name}}\"}}', NULL),
(164, 171, 'directus_permissions', '37', '{\"role\": null, \"action\": \"create\", \"collection\": \"directus_users\"}', '{\"role\": null, \"action\": \"create\", \"collection\": \"directus_users\"}', NULL),
(165, 172, 'directus_permissions', '37', '{\"id\": 37, \"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": null, \"permissions\": null}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": null, \"permissions\": null}', NULL),
(166, 173, 'directus_users', 'e6d6370c-b48f-4216-9460-66a80244b90c', '{\"email\": \"eimepe01@gmail.com\"}', '{\"email\": \"eimepe01@gmail.com\"}', NULL),
(167, 175, 'directus_permissions', '38', '{\"role\": null, \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(168, 176, 'directus_permissions', '39', '{\"role\": null, \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(169, 178, 'directus_permissions', '37', '{\"id\": 37, \"role\": null, \"action\": \"create\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(170, 179, 'directus_permissions', '39', '{\"id\": 39, \"role\": null, \"action\": \"update\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(171, 177, 'directus_permissions', '38', '{\"id\": 38, \"role\": null, \"action\": \"read\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(172, 180, 'directus_permissions', '40', '{\"role\": null, \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(173, 182, 'directus_users', '541fbb17-880d-40c9-a6fd-81452d092211', '{\"email\": \"juan@ddd.com\", \"password\": \"**********\"}', '{\"email\": \"juan@ddd.com\", \"password\": \"**********\"}', NULL),
(174, 183, 'directus_users', '4201194a-2b93-479b-9c30-a525adf569a7', '{\"email\": \"eimepe@hhh.com\", \"password\": \"**********\"}', '{\"email\": \"eimepe@hhh.com\", \"password\": \"**********\"}', NULL),
(175, 184, 'directus_users', 'a8041a7c-9381-450e-bdc0-adc1645ff7e3', '{\"email\": \"dddd@hhh.com\", \"password\": \"**********\"}', '{\"email\": \"dddd@hhh.com\", \"password\": \"**********\"}', NULL),
(176, 189, 'directus_permissions', '41', '{\"role\": null, \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(177, 190, 'directus_permissions', '42', '{\"role\": null, \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(178, 191, 'directus_permissions', '43', '{\"role\": null, \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(179, 192, 'directus_permissions', '44', '{\"role\": null, \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(180, 193, 'directus_users', '2dcd3087-f8dd-49d2-a60b-df83e5ce997b', '{\"email\": \"dddd@hhkk.com\", \"password\": \"**********\"}', '{\"email\": \"dddd@hhkk.com\", \"password\": \"**********\"}', NULL),
(181, 194, 'directus_users', '71cc5666-8e51-4d55-afd7-4f489884ae1e', '{\"email\": \"sdddd@hhkk.com\", \"password\": \"**********\"}', '{\"email\": \"sdddd@hhkk.com\", \"password\": \"**********\"}', NULL),
(182, 195, 'directus_users', '79ea20fd-19f2-46f9-b68b-51d09c4db006', '{\"email\": \"sddddd@hhkk.com\", \"password\": \"**********\"}', '{\"email\": \"sddddd@hhkk.com\", \"password\": \"**********\"}', NULL),
(183, 196, 'directus_users', '1aa13590-3a06-4ebe-8c34-2ccab0732f17', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddddd@hhfsskk.com\", \"password\": \"**********\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddddd@hhfsskk.com\", \"password\": \"**********\"}', NULL),
(184, 200, 'directus_permissions', '41', '{\"id\": 41, \"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"status\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"status\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(185, 201, 'directus_permissions', '41', '{\"id\": 41, \"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(186, 209, 'directus_permissions', '41', '{\"id\": 41, \"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"role\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"role\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(187, 210, 'directus_users', 'b74b722f-af27-48be-8ca3-221f852b99e4', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddddd@hola.com\", \"password\": \"**********\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddddd@hola.com\", \"password\": \"**********\"}', NULL),
(188, 211, 'directus_users', '4cd18351-c105-4276-8d78-16e817b30ed3', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddeddd@hodela.com\", \"password\": \"**********\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"email\": \"sddeddd@hodela.com\", \"password\": \"**********\"}', NULL),
(189, 212, 'directus_fields', '58', '{\"field\": \"type\", \"hidden\": true, \"display\": \"boolean\", \"options\": null, \"special\": \"boolean\", \"readonly\": false, \"interface\": \"boolean\", \"collection\": \"directus_users\", \"display_options\": null}', '{\"field\": \"type\", \"hidden\": true, \"display\": \"boolean\", \"options\": null, \"special\": \"boolean\", \"readonly\": false, \"interface\": \"boolean\", \"collection\": \"directus_users\", \"display_options\": null}', NULL),
(190, 213, 'directus_permissions', '41', '{\"id\": 41, \"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"role\", \"type\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', '{\"role\": null, \"action\": \"create\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"password\", \"role\", \"type\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": {}, \"permissions\": {}}', NULL),
(191, 214, 'directus_users', 'ca46efe5-65dd-4786-8517-fa796ca42463', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"sddeeddd@hodela.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"sddeeddd@hodela.com\", \"password\": \"**********\"}', NULL),
(192, 215, 'directus_users', '035956df-5b71-4bf0-84b9-eabff5ac63e2', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"type\": false, \"email\": \"prueba@hodela.com\", \"password\": \"**********\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"type\": false, \"email\": \"prueba@hodela.com\", \"password\": \"**********\"}', NULL),
(193, 220, 'directus_users', 'b7b0f2b1-a96c-4b24-9371-dadef7f83cad', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"type\": false, \"email\": \"prueba@hodela.com\", \"password\": \"**********\"}', '{\"role\": \"3c6c3c13-6207-48b3-8d6f-0359b252783e\", \"type\": false, \"email\": \"prueba@hodela.com\", \"password\": \"**********\"}', NULL),
(194, 221, 'directus_users', '691fb13e-063e-45f3-806d-c9e6fe2cb48c', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@hwodela.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@hwodela.com\", \"password\": \"**********\"}', NULL),
(195, 222, 'directus_users', '1bfa55b5-9f49-4490-88ad-8fd036454faa', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@hwwodela.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@hwwodela.com\", \"password\": \"**********\"}', NULL),
(196, 223, 'directus_users', '2f69a561-77fe-4a29-be6b-c10223ab0fa2', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@hwwodela.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@hwwodela.com\", \"password\": \"**********\"}', NULL),
(197, 224, 'directus_users', 'e3e7b677-5de3-4dc2-aa72-1f3abd084dc8', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@e.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@e.com\", \"password\": \"**********\"}', NULL),
(198, 225, 'directus_users', '3e783461-ac88-44cd-8631-8d36a84c79b5', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@ew.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewba@ew.com\", \"password\": \"**********\"}', NULL),
(199, 226, 'directus_permissions', '45', '{\"role\": null, \"action\": \"read\", \"collection\": \"directus_users\"}', '{\"role\": null, \"action\": \"read\", \"collection\": \"directus_users\"}', NULL),
(200, 228, 'directus_permissions', '46', '{\"role\": null, \"action\": \"read\", \"collection\": \"directus_users\"}', '{\"role\": null, \"action\": \"read\", \"collection\": \"directus_users\"}', NULL),
(201, 229, 'directus_permissions', '46', '{\"id\": 46, \"role\": null, \"action\": \"read\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"type\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": null, \"permissions\": null}', '{\"role\": null, \"action\": \"read\", \"fields\": [\"first_name\", \"last_name\", \"email\", \"type\"], \"presets\": null, \"collection\": \"directus_users\", \"validation\": null, \"permissions\": null}', NULL),
(202, 230, 'directus_users', '150cce88-2db2-473e-9618-d5e746946d7c', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewwba@ew.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"pruewwba@ew.com\", \"password\": \"**********\"}', NULL),
(203, 231, 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@yahoo.com\", \"password\": \"**********\"}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"type\": true, \"email\": \"prueba@yahoo.com\", \"password\": \"**********\"}', NULL),
(204, 233, 'directus_users', '1b9851da-0586-493e-8ed3-311ed3c0185e', '{\"id\": \"1b9851da-0586-493e-8ed3-311ed3c0185e\", \"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"tags\": null, \"type\": true, \"email\": \"prueba@yahoo.com\", \"theme\": \"auto\", \"title\": null, \"token\": null, \"avatar\": null, \"status\": \"active\", \"trabajo\": [], \"language\": \"es-419\", \"location\": null, \"password\": \"**********\", \"calificar\": [], \"last_name\": null, \"last_page\": \"/users/1b9851da-0586-493e-8ed3-311ed3c0185e\", \"servicios\": [], \"first_name\": null, \"tfa_secret\": null, \"description\": null, \"last_access\": \"2021-09-19T05:45:32.000Z\", \"trabajopedido\": []}', '{\"language\": \"es-419\"}', NULL),
(205, 235, 'directus_permissions', '47', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', NULL),
(206, 236, 'directus_permissions', '48', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', NULL),
(207, 237, 'directus_permissions', '49', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', NULL),
(208, 238, 'directus_permissions', '50', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', NULL),
(209, 243, 'directus_permissions', '48', '{\"id\": 48, \"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"read\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(210, 242, 'directus_permissions', '47', '{\"id\": 47, \"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"create\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(211, 244, 'directus_permissions', '51', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"delete\", \"fields\": [\"*\"], \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', NULL),
(212, 245, 'directus_permissions', '49', '{\"id\": 49, \"role\": \"5c794a99-9d0b-462a-8b0f-747e53434378\", \"action\": \"update\", \"fields\": [\"*\"], \"presets\": null, \"collection\": \"trabajo\", \"validation\": {}, \"permissions\": {}}', '{\"fields\": [\"*\"], \"validation\": {}, \"permissions\": {}}', NULL),
(213, 247, 'directus_settings', '1', '{\"id\": 1, \"basemaps\": null, \"custom_css\": null, \"mapbox_key\": null, \"module_bar\": null, \"project_url\": null, \"public_note\": null, \"project_logo\": null, \"project_name\": \"Servicios\", \"project_color\": \"#F41F1F\", \"public_background\": null, \"public_foreground\": null, \"auth_login_attempts\": 25, \"auth_password_policy\": null, \"storage_asset_presets\": null, \"storage_default_folder\": null, \"storage_asset_transform\": \"all\"}', '{\"project_logo\": null}', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_roles`
--

CREATE TABLE `directus_roles` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL DEFAULT 'supervised_user_circle',
  `description` text,
  `ip_access` text,
  `enforce_tfa` tinyint(1) NOT NULL DEFAULT '0',
  `collection_list` json DEFAULT NULL,
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  `app_access` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_roles`
--

INSERT INTO `directus_roles` (`id`, `name`, `icon`, `description`, `ip_access`, `enforce_tfa`, `collection_list`, `admin_access`, `app_access`) VALUES
('3c6c3c13-6207-48b3-8d6f-0359b252783e', 'tecnico', 'supervised_user_circle', NULL, NULL, 0, NULL, 0, 1),
('5c794a99-9d0b-462a-8b0f-747e53434378', 'cliente', 'supervised_user_circle', NULL, NULL, 0, NULL, 0, 1),
('baa19142-4f26-4649-af61-887000bc9b33', 'Administrator', 'verified', 'Initial administrative role with unrestricted App/API access', NULL, 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_sessions`
--

CREATE TABLE `directus_sessions` (
  `token` varchar(64) NOT NULL,
  `user` char(36) NOT NULL,
  `expires` timestamp NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_sessions`
--

INSERT INTO `directus_sessions` (`token`, `user`, `expires`, `ip`, `user_agent`) VALUES
('ainTCX76Q1AFODL0KrE7O59fDsWlQaxvGCEmUa-3piR9d8QHYyneHFRG0r_Bnm86', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-26 00:32:29', '::1', NULL),
('MNEiAjJw9LfZ0JcjNNvjF9bp8_7y179Um3s4CdMGydvKWTeEh0iLE7GVAeMFrL8R', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-26 06:03:31', '::ffff:127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0'),
('rg3jTH5AgWl0iBbCzpcI5uMGCNXC23MiG0o-xMAh2p9njTQLAZT__990N9nsw8pF', '1b9851da-0586-493e-8ed3-311ed3c0185e', '2021-09-26 06:04:18', '::1', NULL),
('w-gi28cxf5haTfROYKSRQl6UnZW1NnyVT5U3e3xTNo6llMoO0N58MRb45c6pXA9k', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-26 00:02:04', '::1', NULL),
('W4H_7d1SBkwOzwgEr4QWH57OESRYk8ho3dMcvcHZBucytYS-6nIcDtLHoJWu1DzA', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-26 06:08:13', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_settings`
--

CREATE TABLE `directus_settings` (
  `id` int UNSIGNED NOT NULL,
  `project_name` varchar(100) NOT NULL DEFAULT 'Directus',
  `project_url` varchar(255) DEFAULT NULL,
  `project_color` varchar(10) DEFAULT '#00C897',
  `project_logo` char(36) DEFAULT NULL,
  `public_foreground` char(36) DEFAULT NULL,
  `public_background` char(36) DEFAULT NULL,
  `public_note` text,
  `auth_login_attempts` int UNSIGNED DEFAULT '25',
  `auth_password_policy` varchar(100) DEFAULT NULL,
  `storage_asset_transform` varchar(7) DEFAULT 'all',
  `storage_asset_presets` json DEFAULT NULL,
  `custom_css` text,
  `storage_default_folder` char(36) DEFAULT NULL,
  `basemaps` json DEFAULT NULL,
  `mapbox_key` varchar(255) DEFAULT NULL,
  `module_bar` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_settings`
--

INSERT INTO `directus_settings` (`id`, `project_name`, `project_url`, `project_color`, `project_logo`, `public_foreground`, `public_background`, `public_note`, `auth_login_attempts`, `auth_password_policy`, `storage_asset_transform`, `storage_asset_presets`, `custom_css`, `storage_default_folder`, `basemaps`, `mapbox_key`, `module_bar`) VALUES
(1, 'Servicios', NULL, '#F41F1F', NULL, NULL, NULL, NULL, 25, NULL, 'all', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_users`
--

CREATE TABLE `directus_users` (
  `id` char(36) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `tags` json DEFAULT NULL,
  `avatar` char(36) DEFAULT NULL,
  `language` varchar(8) DEFAULT 'en-US',
  `theme` varchar(20) DEFAULT 'auto',
  `tfa_secret` varchar(255) DEFAULT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'active',
  `role` char(36) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `last_access` timestamp NULL DEFAULT NULL,
  `last_page` varchar(255) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `directus_users`
--

INSERT INTO `directus_users` (`id`, `first_name`, `last_name`, `email`, `password`, `location`, `title`, `description`, `tags`, `avatar`, `language`, `theme`, `tfa_secret`, `status`, `role`, `token`, `last_access`, `last_page`, `type`) VALUES
('0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', 'Admin', 'User', 'eimepe@yahoo.com', '$argon2i$v=19$m=4096,t=3,p=1$UrYHx2IErZqw5ZqmW62mEw$hi6M0huuqTfzOh+qcznmIvJ3qg0NvCx9rlpzx6DEPZE', NULL, NULL, NULL, NULL, NULL, 'es-419', 'dark', NULL, 'active', 'baa19142-4f26-4649-af61-887000bc9b33', NULL, '2021-09-19 06:08:13', '/settings/roles/5c794a99-9d0b-462a-8b0f-747e53434378', 1),
('150cce88-2db2-473e-9618-d5e746946d7c', NULL, NULL, 'pruewwba@ew.com', '$argon2i$v=19$m=4096,t=3,p=1$UuvlttmCoPZ7Ooe4Q9LiCw$h8rHFNv6zaarXx1zY7o/NLpqgTZmLYjmpj8kr6ixtfc', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('1b9851da-0586-493e-8ed3-311ed3c0185e', NULL, NULL, 'prueba@yahoo.com', '$argon2i$v=19$m=4096,t=3,p=1$JijYCnQR1J1twadjCWfiPg$fxcsGwwbTFmpNOw3A4CU5PefWYsHh4QkQvn0E8DUTUs', NULL, NULL, NULL, NULL, NULL, 'es-419', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, '2021-09-19 06:04:18', '/collections/trabajo/1', 1),
('1bfa55b5-9f49-4490-88ad-8fd036454faa', NULL, NULL, 'prueba@hwwodela.com', '$argon2i$v=19$m=4096,t=3,p=1$xOT5Rb4dhUxROWSJblM1LA$XweTgOMa7KQ17Tsuj4dFF/IsnnIJtPBrEkY0XSWFvj0', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('2d7e2aee-25f3-46ec-ba96-399561b0f19b', 'eider', 'mejia', 'eimepe2012@gmail.com', '$argon2i$v=19$m=4096,t=3,p=1$LC0mYPqG6bNfPuuSIghyKQ$ikWWpGgT+Ozp4uVkhQKGQakAG3bw78eOt7clN9kczHQ', NULL, NULL, NULL, NULL, NULL, 'es-419', 'auto', NULL, 'active', '3c6c3c13-6207-48b3-8d6f-0359b252783e', NULL, '2021-09-18 23:23:05', '/files', 1),
('2f69a561-77fe-4a29-be6b-c10223ab0fa2', NULL, NULL, 'pruewba@hwwodela.com', '$argon2i$v=19$m=4096,t=3,p=1$BSgog7UKUiVoNZDuuMYFWw$nz+OosYMofWwkUZb0q3ZbSPbdHjUcViRD3EWTOvYKUw', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('3e783461-ac88-44cd-8631-8d36a84c79b5', NULL, NULL, 'pruewba@ew.com', '$argon2i$v=19$m=4096,t=3,p=1$P2sCHyrs/vYrsvpw5kipCw$brJnvsWygGDU2xA99KmDNcfMLiTM5XssciBdZ8+mbtA', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('691fb13e-063e-45f3-806d-c9e6fe2cb48c', NULL, NULL, 'prueba@hwodela.com', '$argon2i$v=19$m=4096,t=3,p=1$i6m7G4dIyy2Y5JWeVEj8ig$yliBb2VrAjm5Pp7c8ike0OQZ7KiIPgd1IRAAMTyESLI', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('b7b0f2b1-a96c-4b24-9371-dadef7f83cad', NULL, NULL, 'prueba@hodela.com', '$argon2i$v=19$m=4096,t=3,p=1$EbWJuUEeEGU7DwJP1WNTMA$gAIpLuByDezACpv8HWBWKJIo1gCeDZ1DYRmgq5wdxmk', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '3c6c3c13-6207-48b3-8d6f-0359b252783e', NULL, NULL, NULL, 0),
('e3e7b677-5de3-4dc2-aa72-1f3abd084dc8', NULL, NULL, 'pruewba@e.com', '$argon2i$v=19$m=4096,t=3,p=1$mgp1am883CjJ1DkGgYEmPw$csS5x3sntLKMm2ODyAPtNV8HEAQSSO7F9WtLcxLAKDM', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '5c794a99-9d0b-462a-8b0f-747e53434378', NULL, NULL, NULL, 1),
('e6d6370c-b48f-4216-9460-66a80244b90c', NULL, NULL, 'eimepe01@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directus_webhooks`
--

CREATE TABLE `directus_webhooks` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `method` varchar(10) NOT NULL DEFAULT 'POST',
  `url` text,
  `status` varchar(10) NOT NULL DEFAULT 'active',
  `data` tinyint(1) NOT NULL DEFAULT '1',
  `actions` varchar(100) NOT NULL,
  `collections` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int UNSIGNED NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'draft',
  `user_created` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `user_updated` char(36) DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `img` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `status`, `user_created`, `date_created`, `user_updated`, `date_updated`, `nombre`, `img`) VALUES
(1, 'published', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 21:59:01', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:22:39', 'mantenimiento de camaras', '778eac17-46b3-48d6-bc0b-8de2daba103b'),
(3, 'published', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 22:23:00', NULL, NULL, 'instalacion camaras', '7eba442d-8249-4b56-a207-c7263fe01c82');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_categorias`
--

CREATE TABLE `servicios_categorias` (
  `id` int UNSIGNED NOT NULL,
  `categorias_id` int UNSIGNED DEFAULT NULL,
  `servicios_id` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `servicios_categorias`
--

INSERT INTO `servicios_categorias` (`id`, `categorias_id`, `servicios_id`) VALUES
(5, 1, 1),
(6, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_directus_users`
--

CREATE TABLE `servicios_directus_users` (
  `id` int UNSIGNED NOT NULL,
  `servicios_id` int UNSIGNED DEFAULT NULL,
  `directus_users_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo`
--

CREATE TABLE `trabajo` (
  `id` int UNSIGNED NOT NULL,
  `user_created` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `user_updated` char(36) DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `descripcion` text,
  `pedidopor` char(36) DEFAULT NULL,
  `realizadopor` char(36) DEFAULT NULL,
  `estado` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `trabajo`
--

INSERT INTO `trabajo` (`id`, `user_created`, `date_created`, `user_updated`, `date_updated`, `descripcion`, `pedidopor`, `realizadopor`, `estado`) VALUES
(1, '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2021-09-18 23:48:18', NULL, NULL, 'esto es un trabajo de prueba', '0e4cadf3-72b7-4f4b-b8e0-c9e184e5d2f3', '2d7e2aee-25f3-46ec-ba96-399561b0f19b', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo_directus_files`
--

CREATE TABLE `trabajo_directus_files` (
  `id` int UNSIGNED NOT NULL,
  `trabajo_id` int UNSIGNED DEFAULT NULL,
  `directus_files_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `trabajo_directus_files`
--

INSERT INTO `trabajo_directus_files` (`id`, `trabajo_id`, `directus_files_id`) VALUES
(1, 1, '7eba442d-8249-4b56-a207-c7263fe01c82'),
(2, 1, '3a3139d9-198e-4193-90e8-cfbcd4a495b1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificar`
--
ALTER TABLE `calificar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `calificar_user_foreign` (`user`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorias_user_created_foreign` (`user_created`),
  ADD KEY `categorias_user_updated_foreign` (`user_updated`),
  ADD KEY `categorias_img_foreign` (`img`);

--
-- Indices de la tabla `directus_activity`
--
ALTER TABLE `directus_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_activity_collection_foreign` (`collection`);

--
-- Indices de la tabla `directus_collections`
--
ALTER TABLE `directus_collections`
  ADD PRIMARY KEY (`collection`);

--
-- Indices de la tabla `directus_fields`
--
ALTER TABLE `directus_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_fields_collection_foreign` (`collection`),
  ADD KEY `directus_fields_group_foreign` (`group`);

--
-- Indices de la tabla `directus_files`
--
ALTER TABLE `directus_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_files_uploaded_by_foreign` (`uploaded_by`),
  ADD KEY `directus_files_modified_by_foreign` (`modified_by`),
  ADD KEY `directus_files_folder_foreign` (`folder`);

--
-- Indices de la tabla `directus_folders`
--
ALTER TABLE `directus_folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_folders_parent_foreign` (`parent`);

--
-- Indices de la tabla `directus_migrations`
--
ALTER TABLE `directus_migrations`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `directus_permissions`
--
ALTER TABLE `directus_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_permissions_collection_foreign` (`collection`),
  ADD KEY `directus_permissions_role_foreign` (`role`);

--
-- Indices de la tabla `directus_presets`
--
ALTER TABLE `directus_presets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_presets_collection_foreign` (`collection`),
  ADD KEY `directus_presets_user_foreign` (`user`),
  ADD KEY `directus_presets_role_foreign` (`role`);

--
-- Indices de la tabla `directus_relations`
--
ALTER TABLE `directus_relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_relations_many_collection_foreign` (`many_collection`),
  ADD KEY `directus_relations_one_collection_foreign` (`one_collection`);

--
-- Indices de la tabla `directus_revisions`
--
ALTER TABLE `directus_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_revisions_collection_foreign` (`collection`),
  ADD KEY `directus_revisions_parent_foreign` (`parent`),
  ADD KEY `directus_revisions_activity_foreign` (`activity`);

--
-- Indices de la tabla `directus_roles`
--
ALTER TABLE `directus_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `directus_sessions`
--
ALTER TABLE `directus_sessions`
  ADD PRIMARY KEY (`token`),
  ADD KEY `directus_sessions_user_foreign` (`user`);

--
-- Indices de la tabla `directus_settings`
--
ALTER TABLE `directus_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directus_settings_project_logo_foreign` (`project_logo`),
  ADD KEY `directus_settings_public_foreground_foreign` (`public_foreground`),
  ADD KEY `directus_settings_public_background_foreign` (`public_background`),
  ADD KEY `directus_settings_storage_default_folder_foreign` (`storage_default_folder`);

--
-- Indices de la tabla `directus_users`
--
ALTER TABLE `directus_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `directus_users_email_unique` (`email`),
  ADD KEY `directus_users_role_foreign` (`role`);

--
-- Indices de la tabla `directus_webhooks`
--
ALTER TABLE `directus_webhooks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicios_user_created_foreign` (`user_created`),
  ADD KEY `servicios_user_updated_foreign` (`user_updated`),
  ADD KEY `servicios_img_foreign` (`img`);

--
-- Indices de la tabla `servicios_categorias`
--
ALTER TABLE `servicios_categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicios_categorias_servicios_id_foreign` (`servicios_id`),
  ADD KEY `servicios_categorias_categorias_id_foreign` (`categorias_id`);

--
-- Indices de la tabla `servicios_directus_users`
--
ALTER TABLE `servicios_directus_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicios_directus_users_servicios_id_foreign` (`servicios_id`),
  ADD KEY `servicios_directus_users_directus_users_id_foreign` (`directus_users_id`);

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trabajo_user_created_foreign` (`user_created`),
  ADD KEY `trabajo_user_updated_foreign` (`user_updated`),
  ADD KEY `trabajo_pedidopor_foreign` (`pedidopor`),
  ADD KEY `trabajo_realizadopor_foreign` (`realizadopor`);

--
-- Indices de la tabla `trabajo_directus_files`
--
ALTER TABLE `trabajo_directus_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trabajo_directus_files_directus_files_id_foreign` (`directus_files_id`),
  ADD KEY `trabajo_directus_files_trabajo_id_foreign` (`trabajo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `calificar`
--
ALTER TABLE `calificar`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `directus_activity`
--
ALTER TABLE `directus_activity`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT de la tabla `directus_fields`
--
ALTER TABLE `directus_fields`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `directus_permissions`
--
ALTER TABLE `directus_permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `directus_presets`
--
ALTER TABLE `directus_presets`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `directus_relations`
--
ALTER TABLE `directus_relations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `directus_revisions`
--
ALTER TABLE `directus_revisions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT de la tabla `directus_settings`
--
ALTER TABLE `directus_settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `directus_webhooks`
--
ALTER TABLE `directus_webhooks`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `servicios_categorias`
--
ALTER TABLE `servicios_categorias`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `servicios_directus_users`
--
ALTER TABLE `servicios_directus_users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `trabajo_directus_files`
--
ALTER TABLE `trabajo_directus_files`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `calificar`
--
ALTER TABLE `calificar`
  ADD CONSTRAINT `calificar_user_foreign` FOREIGN KEY (`user`) REFERENCES `directus_users` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `categorias_img_foreign` FOREIGN KEY (`img`) REFERENCES `directus_files` (`id`),
  ADD CONSTRAINT `categorias_user_created_foreign` FOREIGN KEY (`user_created`) REFERENCES `directus_users` (`id`),
  ADD CONSTRAINT `categorias_user_updated_foreign` FOREIGN KEY (`user_updated`) REFERENCES `directus_users` (`id`);

--
-- Filtros para la tabla `directus_fields`
--
ALTER TABLE `directus_fields`
  ADD CONSTRAINT `directus_fields_group_foreign` FOREIGN KEY (`group`) REFERENCES `directus_fields` (`id`);

--
-- Filtros para la tabla `directus_files`
--
ALTER TABLE `directus_files`
  ADD CONSTRAINT `directus_files_folder_foreign` FOREIGN KEY (`folder`) REFERENCES `directus_folders` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `directus_files_modified_by_foreign` FOREIGN KEY (`modified_by`) REFERENCES `directus_users` (`id`),
  ADD CONSTRAINT `directus_files_uploaded_by_foreign` FOREIGN KEY (`uploaded_by`) REFERENCES `directus_users` (`id`);

--
-- Filtros para la tabla `directus_folders`
--
ALTER TABLE `directus_folders`
  ADD CONSTRAINT `directus_folders_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `directus_folders` (`id`);

--
-- Filtros para la tabla `directus_permissions`
--
ALTER TABLE `directus_permissions`
  ADD CONSTRAINT `directus_permissions_role_foreign` FOREIGN KEY (`role`) REFERENCES `directus_roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `directus_presets`
--
ALTER TABLE `directus_presets`
  ADD CONSTRAINT `directus_presets_role_foreign` FOREIGN KEY (`role`) REFERENCES `directus_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directus_presets_user_foreign` FOREIGN KEY (`user`) REFERENCES `directus_users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `directus_revisions`
--
ALTER TABLE `directus_revisions`
  ADD CONSTRAINT `directus_revisions_activity_foreign` FOREIGN KEY (`activity`) REFERENCES `directus_activity` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directus_revisions_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `directus_revisions` (`id`);

--
-- Filtros para la tabla `directus_sessions`
--
ALTER TABLE `directus_sessions`
  ADD CONSTRAINT `directus_sessions_user_foreign` FOREIGN KEY (`user`) REFERENCES `directus_users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `directus_settings`
--
ALTER TABLE `directus_settings`
  ADD CONSTRAINT `directus_settings_project_logo_foreign` FOREIGN KEY (`project_logo`) REFERENCES `directus_files` (`id`),
  ADD CONSTRAINT `directus_settings_public_background_foreign` FOREIGN KEY (`public_background`) REFERENCES `directus_files` (`id`),
  ADD CONSTRAINT `directus_settings_public_foreground_foreign` FOREIGN KEY (`public_foreground`) REFERENCES `directus_files` (`id`),
  ADD CONSTRAINT `directus_settings_storage_default_folder_foreign` FOREIGN KEY (`storage_default_folder`) REFERENCES `directus_folders` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `directus_users`
--
ALTER TABLE `directus_users`
  ADD CONSTRAINT `directus_users_role_foreign` FOREIGN KEY (`role`) REFERENCES `directus_roles` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD CONSTRAINT `servicios_img_foreign` FOREIGN KEY (`img`) REFERENCES `directus_files` (`id`),
  ADD CONSTRAINT `servicios_user_created_foreign` FOREIGN KEY (`user_created`) REFERENCES `directus_users` (`id`),
  ADD CONSTRAINT `servicios_user_updated_foreign` FOREIGN KEY (`user_updated`) REFERENCES `directus_users` (`id`);

--
-- Filtros para la tabla `servicios_categorias`
--
ALTER TABLE `servicios_categorias`
  ADD CONSTRAINT `servicios_categorias_categorias_id_foreign` FOREIGN KEY (`categorias_id`) REFERENCES `categorias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `servicios_categorias_servicios_id_foreign` FOREIGN KEY (`servicios_id`) REFERENCES `servicios` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `servicios_directus_users`
--
ALTER TABLE `servicios_directus_users`
  ADD CONSTRAINT `servicios_directus_users_directus_users_id_foreign` FOREIGN KEY (`directus_users_id`) REFERENCES `directus_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `servicios_directus_users_servicios_id_foreign` FOREIGN KEY (`servicios_id`) REFERENCES `servicios` (`id`) ON DELETE SET NULL;

--
-- Filtros para la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD CONSTRAINT `trabajo_pedidopor_foreign` FOREIGN KEY (`pedidopor`) REFERENCES `directus_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `trabajo_realizadopor_foreign` FOREIGN KEY (`realizadopor`) REFERENCES `directus_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `trabajo_user_created_foreign` FOREIGN KEY (`user_created`) REFERENCES `directus_users` (`id`),
  ADD CONSTRAINT `trabajo_user_updated_foreign` FOREIGN KEY (`user_updated`) REFERENCES `directus_users` (`id`);

--
-- Filtros para la tabla `trabajo_directus_files`
--
ALTER TABLE `trabajo_directus_files`
  ADD CONSTRAINT `trabajo_directus_files_directus_files_id_foreign` FOREIGN KEY (`directus_files_id`) REFERENCES `directus_files` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `trabajo_directus_files_trabajo_id_foreign` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

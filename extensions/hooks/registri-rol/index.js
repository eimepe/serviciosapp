module.exports = function registerHook({ services, exceptions }) {
  const { ServiceUnavailableException, ForbiddenException } = exceptions;

  return {
    "users.create.before": async function (input, { collection, schema }) {
      console.log();
      if (input.role) {
        throw new ForbiddenException();
      }
      if (input.type == true) {
        console.log("es cliente");
        input.role = "5c794a99-9d0b-462a-8b0f-747e53434378";
        return input;
      } else if (input.type == false) {
        input.role = "3c6c3c13-6207-48b3-8d6f-0359b252783e";
        return input;
      } else {
        throw new ForbiddenException();
      }

      return input;
    },
  };
};
